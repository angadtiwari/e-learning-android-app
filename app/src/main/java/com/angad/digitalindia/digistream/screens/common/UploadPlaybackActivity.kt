package com.angad.digitalindia.digistream.screens.common

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.components.AppDialogView
import com.angad.digitalindia.digistream.constants.AppEventsConstants
import com.angad.digitalindia.digistream.extensions.appActivityDialog
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.models.Category
import com.angad.digitalindia.digistream.models.eventbus.Events
import com.angad.digitalindia.digistream.screens.common.zoomview.ZoomVideoActivity
import com.angad.digitalindia.digistream.streaming.AppStreamManager
import com.angad.digitalindia.digistream.streaming.AppStreamingUrls
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipDrawable
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.activity_upload_playback.*
import kotlinx.android.synthetic.main.toolbar_app_activity.*
import net.alhazmy13.mediapicker.Video.VideoPicker
import org.greenrobot.eventbus.EventBus

class UploadPlaybackActivity: AppBaseActivity() {

    private var choosenCategories: MutableList<Category> = mutableListOf()
    private var choosenVideoPath: String? = null
    private var choosenVideoUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_playback)

        showToolbar = true
        txt_heading.text = "Upload Video"
        btn_close.visibility = View.VISIBLE
        initListeners()
        initCategoryChips()
    }

    private fun initCategoryChips() {
        preference.prefCategories?.let {
            choosenCategories = it.toMutableList()
            choosenCategories.forEach {category ->
                addCategoryChip(chipGroupCategory, category)
            }
        }
    }

    private fun addCategoryChip(chipGroup: ChipGroup, category: Category) {
        val chip = Chip(this)
        chip.setChipDrawable(ChipDrawable.createFromResource(this, R.xml.chip_category))
        chip.text = category.name
        chip.isChecked = false
        chip.setOnCheckedChangeListener { compoundButton, b -> category.selected = b }
        chipGroup.addView(chip)
    }

    private fun initListeners() {
        player_choose_video.setOnClickListener {
            choosenVideoUri?.path?.let {
                val intent = Intent(this, ZoomVideoActivity::class.java)
                intent.putExtra("playbackUrl", it)
                startActivity(intent)
            }
        }

        btn_play_video.setOnClickListener {
            choosenVideoUri?.path?.let {
                val intent = Intent(this, ZoomVideoActivity::class.java)
                intent.putExtra("playbackUrl", it)
                startActivity(intent)
            }
        }

        btn_choose_video.setOnClickListener {
            VideoPicker.Builder(this)
                .mode(VideoPicker.Mode.GALLERY)
                .directory(VideoPicker.Directory.DEFAULT)
                .extension(VideoPicker.Extension.MP4)
                .enableDebuggingMode(true)
                .build()
        }

        btn_continue.setOnClickListener {
            if (edit_playback_title.text.trim().toString().isNullOrBlank()) {
                showFieldError("Title required...")
                return@setOnClickListener
            }
            if (choosenCategories.firstOrNull { it.selected }?._id == null) {
                showFieldError("Category required...")
                return@setOnClickListener
            }
            if (choosenVideoUri == null) {
                showFieldError("Video required...")
                return@setOnClickListener
            }

            val bundle = Bundle()
            bundle.putString("title", edit_playback_title.text.trim().toString())
            bundle.putString("category", choosenCategories.firstOrNull { it.selected }?._id)
            bundle.putString("playback", choosenVideoUri!!.path)
            EventBus.getDefault().post(Events(AppEventsConstants.EVENT_UPLOADING_STARTED, bundle))

            finish()
        }
    }

    private fun showFieldError(msg: String) {
        appActivityDialog(resources.getString(R.string.app_name), msg, AppDialogView.ERROR)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            VideoPicker.VIDEO_PICKER_REQUEST_CODE -> {
                if(resultCode == Activity.RESULT_OK) {
                    val mPaths: List<String>? = data?.getStringArrayListExtra(VideoPicker.EXTRA_VIDEO_PATH)?.toList()
                    if(mPaths.isNullOrEmpty().not()) {
                        choosenVideoPath = mPaths?.get(0)
                        choosenVideoUri = Uri.parse(choosenVideoPath)

                        btn_play_video.visibility = View.VISIBLE
                        btn_choose_video.text = "Change Video"
                        val streamingUrls = AppStreamingUrls(localUri = choosenVideoUri)
                        AppStreamManager(this).initUploadMediaPlayer(playerView = player_choose_video, streamingUrls = streamingUrls)
                    }
                }
            }
        }
    }

    override fun onMessageEvent(events: Events?) {
        super.onMessageEvent(events)
    }
}