package com.angad.digitalindia.digistream.screens.onboarding.fragments

import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseFragment
import com.angad.digitalindia.digistream.callbacks.IOnGenericCallback
import com.angad.digitalindia.digistream.components.AppDialogView
import com.angad.digitalindia.digistream.constants.AppEventsConstants
import com.angad.digitalindia.digistream.constants.AppSocialAccountConstants
import com.angad.digitalindia.digistream.extensions.appFragmentDialog
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.models.Category
import com.angad.digitalindia.digistream.models.ProfileType
import com.angad.digitalindia.digistream.models.User
import com.angad.digitalindia.digistream.models.eventbus.Events
import com.angad.digitalindia.digistream.networks.AppNetworkingService
import com.angad.digitalindia.digistream.networks.models.ApiProfile
import com.angad.digitalindia.digistream.screens.dashboard.DashboardActivity
import com.angad.digitalindia.digistream.screens.onboarding.OnboardingActivity
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipDrawable
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.fragment_category.*
import kotlinx.coroutines.*
import org.greenrobot.eventbus.EventBus

class CategoryFragment: AppBaseFragment() {

    var categories: MutableList<Category> = mutableListOf()
    var profileTypes: MutableList<ProfileType> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initListeners()

        requireActivity().preference.prefAppUser?.let {
            if (it.provider != AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
                initProfileTypes()
            }
        }
        initCategoryChips()
    }

    private fun initProfileTypes() {
        txt_profile_type.visibility = View.VISIBLE
        txt_profile_type_hint.visibility = View.VISIBLE
        header_border.visibility = View.VISIBLE

        GlobalScope.launch(Dispatchers.Main) {
            if(requireActivity().preference.prefCategories.isNullOrEmpty()) {
                apiImplementation?.fetchProfileTypes(object: IOnGenericCallback {
                    override fun onSuccess() {
                        profileTypes = context?.preference?.prefProfileTypes!!.toMutableList()
                        profileTypes.forEach { profileType ->
                            addProfileTypeChip(chipGroupProfileType, profileType)
                        }
                    }

                    override fun onFailure() {

                    }
                })
            } else {
                profileTypes = context?.preference?.prefProfileTypes!!.toMutableList()
                profileTypes.forEach { profileType ->
                    addProfileTypeChip(chipGroupProfileType, profileType)
                }
            }
        }
    }

    private fun initCategoryChips() {
        GlobalScope.launch(Dispatchers.Main) {
            if(requireActivity().preference.prefCategories.isNullOrEmpty()) {
                apiImplementation?.fetchCategories(object: IOnGenericCallback {
                    override fun onSuccess() {
                        categories = context?.preference?.prefCategories!!.toMutableList()
                        categories.forEach { category ->
                            addCategoryChip(chipGroupCategory, category)
                        }
                    }

                    override fun onFailure() {

                    }
                })
            } else {
                categories = context?.preference?.prefCategories!!.toMutableList()
                categories.forEach { category ->
                    addCategoryChip(chipGroupCategory, category)
                }
            }
        }
    }

    private fun addProfileTypeChip(chipGroup: ChipGroup, profileType: ProfileType) {
        val chip = Chip(context)
        chip.setChipDrawable(ChipDrawable.createFromResource(requireContext(), R.xml.chip_profile_type))
        chip.text = profileType.type
        chip.isChecked = profileType.selected
        chip.setOnCheckedChangeListener { compoundButton, b ->
            profileType.selected = b
            btn_continue.isEnabled = enableContinueBtn()
            enableOrganisationNameField()
        }
        chipGroup.addView(chip)
    }

    private fun addCategoryChip(chipGroup: ChipGroup, category: Category) {
        val chip = Chip(context)
        chip.setChipDrawable(ChipDrawable.createFromResource(requireContext(), R.xml.chip_category))
        chip.text = category.name
        chip.isChecked = category.selected
        chip.setOnCheckedChangeListener { compoundButton, b ->
            category.selected = b
            btn_continue.isEnabled = enableContinueBtn()
            enableOrganisationNameField()
        }
        chipGroup.addView(chip)
    }

    private fun enableOrganisationNameField() {
        val enableOrganisationNameField = profileTypes.any { it.required_name && it.selected }
        edit_organisation_name.visibility = if(enableOrganisationNameField) View.VISIBLE else View.GONE
        if(enableOrganisationNameField.not()) {
            edit_organisation_name.setText("")
        }
    }

    private fun enableContinueBtn(): Boolean {
        if (requireActivity().preference.prefAppUser?.provider != AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
            return categories.filter { it.selected }.isNullOrEmpty().not() && profileTypes.filter { it.selected }.isNullOrEmpty().not()
        } else {
            return categories.filter { it.selected }.isNullOrEmpty().not()
        }
    }

    private fun initListeners() {
        btn_continue.setOnClickListener {
            if(requireActivity().preference.prefAppUser?.provider != AppSocialAccountConstants.ProviderName.PROVIDER_GUEST && profileTypes.filter { it.selected }.isNullOrEmpty() && activity?.preference?.prefAppUser?.provider != AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
                appFragmentDialog(resources.getString(R.string.app_name), "Need to know Profile type", AppDialogView.INFO, false)
                return@setOnClickListener
            }
            if(requireActivity().preference.prefAppUser?.provider != AppSocialAccountConstants.ProviderName.PROVIDER_GUEST && profileTypes.filter { it.selected }[0].required_name && edit_organisation_name.text.trim().toString().isEmpty()) {
                appFragmentDialog(resources.getString(R.string.app_name), "Need to know Organization Name", AppDialogView.INFO, false)
                return@setOnClickListener
            }
            if(categories.filter { it.selected }.isNullOrEmpty()) {
                appFragmentDialog(resources.getString(R.string.app_name), "Choose at least one category you are interested in", AppDialogView.INFO, false)
                return@setOnClickListener
            }
            activity?.let { _activity ->
                if (requireActivity().preference.prefAppUser?.provider == AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
                    val profile = _activity.preference.prefAppUser
                    profile?.let {
                        it.categories = categories.filter { it.selected }.toMutableList()
                        _activity.preference.prefAppUser = it
                    }

                    val intent = Intent(_activity, DashboardActivity::class.java)
                    _activity.startActivity(intent)
                    _activity.finish()
                    _activity.overridePendingTransition(0, android.R.anim.fade_out)

                    return@setOnClickListener
                }

                startLoading()
                EventBus.getDefault().post(Events(AppEventsConstants.EVENT_REGISTRATION))

                val organizationName = edit_organisation_name.text.trim().toString()
                val selectedProfileType = profileTypes.filter { it.selected }[0]
                val selectedCategories = categories.filter { it.selected }.toMutableList()
                val profileRequest = ApiProfile.Request(organizationName, selectedProfileType, selectedCategories)

                GlobalScope.launch(Dispatchers.Main) {
                    val response = withContext(Dispatchers.Default) {
                        AppNetworkingService.instance.updateProfile(profileRequest)
                    }
                    stopLoading()
                    if(response.isSuccessful) {
                        response.body()?.let {
                            _activity.preference.prefAppUser = it.data
                            val intent = Intent(_activity, DashboardActivity::class.java)
                            _activity.startActivity(intent)
                            _activity.finish()
                            _activity.preference.prefAppUser?.provider?.let {
                                if(it != AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
                                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK  or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                }
                            }
                            _activity.overridePendingTransition(0, android.R.anim.fade_out)
                        }
                    } else {
                        appFragmentDialog(resources.getString(R.string.app_name), "Something went wrong while signing you in.", AppDialogView.INFO, false)
                    }
                }
            }
        }
    }

    private fun initView() {
        btn_continue.isEnabled = false
        if(activity?.preference?.prefAppUser?.provider != AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
            layout_profile_type.visibility = View.VISIBLE
        }
    }

    override fun onMessageEvent(events: Events) {
        super.onMessageEvent(events)
        when (events.eventType) {

        }
    }
}