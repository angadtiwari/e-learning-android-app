package com.angad.digitalindia.digistream.notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.callbacks.IOnPushBigPicture
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.screens.common.zoomview.ZoomVideoActivity
import com.angad.digitalindia.digistream.screens.onboarding.OnboardingActivity
import com.facebook.common.executors.CallerThreadExecutor
import com.facebook.common.references.CloseableReference
import com.facebook.datasource.DataSource
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber
import com.facebook.imagepipeline.image.CloseableImage
import com.facebook.imagepipeline.request.ImageRequest
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import org.json.JSONObject


/**
 * Created by Angad Tiwari on 07-07-2020.
 */
class PushMessagingService: FirebaseMessagingService() {

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        preference.prefDeviceToken = token
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        remoteMessage.data.let {
            val notificationPayload = Gson().fromJson(JSONObject(remoteMessage.data as Map<*, *>).toString(), AppNotificationPayload::class.java)
            notificationPayload?.let {
                if(it.media.isNullOrBlank()) {
                    createNotification(it)
                } else {
                    fetchBigPicture(it, object: IOnPushBigPicture{
                        override fun onSuccess(bitmap: Bitmap) {
                            createNotification(it, bitmap)
                        }
                        override fun onFailure() {
                            createNotification(it)
                        }
                    })
                }
            }
        }
    }

    private fun createNotification(notificationPayload: AppNotificationPayload, bitmap: Bitmap? = null) {
        val notificationTitle = notificationPayload.title
        val notificationBody = notificationPayload.body
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val intent = Intent(this, ZoomVideoActivity::class.java)
        intent.putExtra("playbackId", notificationPayload.id)
        intent.putExtra("livestream", (notificationPayload.type == "livestream"))
        val pendingIntent = PendingIntent.getActivity(this, System.currentTimeMillis().toInt(), intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val mBuilder =
            NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher_rounded)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher_rounded))
                .setContentTitle(notificationTitle)
                .setStyle(NotificationCompat.BigTextStyle().bigText(notificationBody))
                .setContentText(notificationBody)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
        bitmap?.let {
            mBuilder.setStyle(NotificationCompat.BigPictureStyle().bigPicture(it))
        }
        if (Build.VERSION.SDK_INT >= 26) {
            val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val mChannel = NotificationChannel("channel", "name", NotificationManager.IMPORTANCE_HIGH)
            mNotificationManager.createNotificationChannel(mChannel)
            mBuilder.setChannelId("channel")
        }
        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.notify(System.currentTimeMillis().toInt(), mBuilder.build())
    }

    // Load bitmap from image url on background thread and display image notification
    private fun fetchBigPicture(appNotificationPayload: AppNotificationPayload, iOnPushBigPicture: IOnPushBigPicture) {
        val imageRequest: ImageRequest = ImageRequestBuilder
            .newBuilderWithSource(Uri.parse(appNotificationPayload.media))
            .setAutoRotateEnabled(true)
            .build()

        val imagePipeline = Fresco.getImagePipeline()
        val dataSource: DataSource<CloseableReference<CloseableImage>> =
            imagePipeline.fetchDecodedImage(imageRequest, this)

        dataSource.subscribe(object : BaseBitmapDataSubscriber() {
            override fun onFailureImpl(dataSource: DataSource<CloseableReference<CloseableImage>>) {
                iOnPushBigPicture.onFailure()
                dataSource.close()
            }

            override fun onNewResultImpl(bitmap: Bitmap?) {
                if (dataSource.isFinished() && bitmap != null) {
                    val bmp = Bitmap.createBitmap(bitmap)
                    iOnPushBigPicture.onSuccess(bmp)
                    dataSource.close()
                }
            }
        }, CallerThreadExecutor.getInstance())
    }
}