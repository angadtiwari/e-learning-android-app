package com.angad.digitalindia.digistream.screens.onboarding

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.angad.digitalindia.digistream.BuildConfig
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.extensions.fullscreen
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.screens.dashboard.DashboardActivity
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppBaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fullscreen()
        setContentView(R.layout.activity_splash)
        showToolbar = false
        bindData()
    }

    override fun onStart() {
        super.onStart()
        Handler().postDelayed({
            if(preference.prefAppUser == null) {
                startActivity(Intent(this@SplashActivity, OnboardingActivity::class.java))
            } else {
                val intent = Intent(this, DashboardActivity::class.java)
                startActivity(intent)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK  or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            finish()
            overridePendingTransition(0, android.R.anim.fade_out)
        }, 1500)
    }

    private fun bindData() {
        textView.text = "Powered by: ${resources.getString(R.string.app_name)} v${BuildConfig.VERSION_NAME} : Digital India"
    }
}