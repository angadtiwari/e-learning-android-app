package com.angad.digitalindia.digistream.auth.socialaccount

import android.content.Intent
import androidx.fragment.app.FragmentActivity
import com.angad.digitalindia.digistream.auth.IAppLoginAuthentication
import com.angad.digitalindia.digistream.auth.IOnAuthCallback
import com.angad.digitalindia.digistream.constants.AppRequestCodeConstants
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.models.User
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions

/**
 * Created by Angad Tiwari on 07-07-2020.
 */
class GuestAuth: IAppLoginAuthentication {

    override fun loginRequest(activity: FragmentActivity) {

    }

    override fun login(activity: FragmentActivity, iOnAuthCallback: IOnAuthCallback?, intent: Intent?) {
        val userProfile = User.Profile(email = "", push_token = activity.preference.prefDeviceToken)
        activity.preference.prefAppUser = userProfile
        iOnAuthCallback?.onSuccess(userProfile)
    }

    override fun logout() {

    }

    companion object {
        val instance: GuestAuth =
            GuestAuth()
    }
}