package com.angad.digitalindia.digistream.base

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.auth.socialaccount.FacebookAuth
import com.angad.digitalindia.digistream.auth.socialaccount.GoogleAuth
import com.angad.digitalindia.digistream.callbacks.IOnGenericCallback
import com.angad.digitalindia.digistream.constants.AppSocialAccountConstants
import com.angad.digitalindia.digistream.extensions.computeDeviceDimension
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.models.eventbus.Events
import com.angad.digitalindia.digistream.networks.AppNetworkingService
import com.angad.digitalindia.digistream.picker.ItemModel
import com.angad.digitalindia.digistream.picker.PickerDialog
import com.angad.digitalindia.digistream.screens.onboarding.OnboardingActivity
import kotlinx.android.synthetic.main.activity_app_base.*
import kotlinx.android.synthetic.main.toolbar_app_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.ArrayList

/**
 * Created by Angad Tiwari on 05-07-2020.
 */
open class AppBaseActivity: AppCompatActivity() {

    private lateinit var pickerDialogBuilder: PickerDialog.Builder
    private lateinit var pickerDialog: PickerDialog
    lateinit var apiImplementation: IApiImplementation

    override fun setContentView(layoutResID: Int) {
        val constraintLayout: ConstraintLayout = layoutInflater.inflate(R.layout.activity_app_base, null) as ConstraintLayout
        val activityContainer: FrameLayout = constraintLayout.findViewById(R.id.layout_container)
        layoutInflater.inflate(layoutResID, activityContainer, true)
        apiImplementation = IApiImplementation(this)
        if(preference.prefDeviceWidth == 0 || preference.prefDeviceHeight == 0) {
            computeDeviceDimension()
        }

        super.setContentView(constraintLayout)
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }

    private fun initListeners() {
        btn_close.setOnClickListener {
            onToolbarLeftOptionClicked(it)
        }
        btn_done.setOnClickListener {
            onToolbarRightOptionClicked(it)
        }
    }

    open fun onToolbarLeftOptionClicked(view: View) {
        finish()
    }

    open fun onToolbarRightOptionClicked(view: View) {

    }

    inline var showToolbar: Boolean
        get() {
            return (toolbar.visibility == View.VISIBLE)
        }
        set(showToolbar) {
            toolbar.visibility = if (showToolbar) View.VISIBLE else View.GONE
        }

    fun requestPhotoPicker(title: String, items: ArrayList<ItemModel>? = null, func: ((type: Long, uri: Uri) -> Unit)? = null) {
        pickerDialogBuilder = PickerDialog.Builder(this)
        pickerDialogBuilder.setTitle(title)
        pickerDialogBuilder.setTitleTextSize(17.0f)
        pickerDialogBuilder.setTitleTextColor(resources.getColor(R.color.colorPrimary))
        pickerDialogBuilder.setListType(PickerDialog.TYPE_LIST)
        if(items != null) {
            pickerDialogBuilder.setItems(items)
        } else {
            pickerDialogBuilder.setItems(
                arrayListOf<ItemModel>(
                    ItemModel(ItemModel.ITEM_CAMERA),
                    ItemModel(ItemModel.ITEM_GALLERY)
                )
            )
        }

        pickerDialogBuilder.setDialogStyle(PickerDialog.DIALOG_MATERIAL)
        pickerDialog = pickerDialogBuilder.create()

        pickerDialog.setPickerCloseListener { type, uri ->
            func?.invoke(type, uri)
        }
        pickerDialog.show(supportFragmentManager, PickerDialog::class.java.simpleName)
    }

    open fun onPhotosReturned(uri: Uri) {
        Log.d("onPhotosReturned", "${uri.path}")
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onMessageEvent(events: Events?) {

    }

    /**
     * setting the internet connection listener and registering the event bus
     */
    override fun onStart() {
        super.onStart()
        initListeners()
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    /**
     * removing the internet connection listener and de-regsitering the event bus
     */
    override fun onDestroy() {
        super.onDestroy()
        if(EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(0, android.R.anim.fade_out)
    }

    open fun clearLoginSession() {
        preference.prefAppUser?.let {
            when(it.provider) {
                AppSocialAccountConstants.ProviderName.PROVIDER_GOOGLE -> GoogleAuth.instance.logout()
                AppSocialAccountConstants.ProviderName.PROVIDER_FACEBOOK -> FacebookAuth.instance.logout()
            }
        }
        preference.clearPref()
        val intent = Intent(this, OnboardingActivity::class.java)
        startActivity(intent)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK  or Intent.FLAG_ACTIVITY_CLEAR_TASK
        finish()
    }

    open fun startLoading() {
        progress_bar.visibility = View.VISIBLE
    }

    open fun stopLoading() {
        progress_bar.visibility = View.GONE
    }

    open class IApiImplementation(val activity: AppBaseActivity) {

        suspend fun fetchCategories(iOnGenericCallback: IOnGenericCallback? = null) {
            if(activity.preference.prefCategories.isNullOrEmpty().not()) {
                return
            }
            GlobalScope.launch(Dispatchers.IO) {
                val response = AppNetworkingService.instance.categories()
                if(response.isSuccessful) {
                    response.body()?.let {
                        activity.preference.prefCategories = it.data
                        GlobalScope.launch(Dispatchers.Main) {
                            iOnGenericCallback?.onSuccess()
                        }
                    }
                } else {
                    GlobalScope.launch(Dispatchers.Main) {
                        iOnGenericCallback?.onFailure()
                    }
                }
            }
        }

        suspend fun fetchProfileTypes(iOnGenericCallback: IOnGenericCallback? = null) {
            if(activity.preference.prefProfileTypes.isNullOrEmpty().not()) {
                return
            }
            GlobalScope.launch(Dispatchers.IO) {
                val response = AppNetworkingService.instance.profileTypes()
                if(response.isSuccessful) {
                    response.body()?.let {
                        activity.preference.prefProfileTypes = it.data
                        GlobalScope.launch(Dispatchers.Main) {
                            iOnGenericCallback?.onSuccess()
                        }
                    }
                } else {
                    GlobalScope.launch(Dispatchers.Main) {
                        iOnGenericCallback?.onFailure()
                    }
                }
            }
        }
    }

    open override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
    }
}