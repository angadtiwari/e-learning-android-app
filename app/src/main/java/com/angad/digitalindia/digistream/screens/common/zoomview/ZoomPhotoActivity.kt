package com.angad.digitalindia.digistream.screens.common.zoomview

import android.os.Bundle
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.extensions.fullscreen
import kotlinx.android.synthetic.main.activity_zoom_photo.*

/**
 * Created by Angad Tiwari on 10-07-2020.
 */
class ZoomPhotoActivity: AppBaseActivity() {

    private var photoUrl: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fullscreen()
        setContentView(R.layout.activity_zoom_photo)
        showToolbar = false

        handleIntent()
        bindData()
    }

    private fun handleIntent() {
        photoUrl = intent.getStringExtra("photo_url")
    }

    private fun bindData() {
        photoUrl?.let {
            zoom_pic.setImageURI(it)
        }
    }
}
