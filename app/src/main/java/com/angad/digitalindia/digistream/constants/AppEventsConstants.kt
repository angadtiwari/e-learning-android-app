package com.angad.digitalindia.digistream.constants

/**
 * Created by Angad Tiwari on 11-07-2020.
 */
class AppEventsConstants {

    companion object {
        const val EVENT_REGISTRATION = "event_registration"
        const val EVENT_CAMERA_PERMISSION_GRANTED = "event_camera_permission_granted"
        const val EVENT_LIVESTREAMING_CREATED = "event_livestreaming_created"

        const val EVENT_UPLOADING_STARTED = "event_uploading_started"
        const val EVENT_UPLOADING_DONE = "event_uploading_done"

        const val NEW_GOLIVE_SESSION = "NEW_GOLIVE_SESSION"
    }
}