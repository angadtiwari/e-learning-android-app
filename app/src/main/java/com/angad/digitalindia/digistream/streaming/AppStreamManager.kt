package com.angad.digitalindia.digistream.streaming

import android.content.Context
import android.net.Uri
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.extensions.cacheUrl
import com.angad.digitalindia.digistream.extensions.cacheUrlAvailable
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.AssetDataSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.EventLogger

/**
 * Created by Angad Tiwari on 07-07-2020.
 */
class AppStreamManager(private val context: Context) {

    private lateinit var player: SimpleExoPlayer
    private var playWhenReady = true
    private var currentWindow = 0
    private var playbackPosition: Long = 200

    fun initOnboardingPlayer(playerView: PlayerView, onboardingStreamingUrls: AppStreamingUrls): SimpleExoPlayer {
        val trackSelector = DefaultTrackSelector()
        trackSelector.setParameters(trackSelector.buildUponParameters().setMaxVideoSizeSd())
        player = ExoPlayerFactory.newSimpleInstance(context, trackSelector)
        playerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM
        playerView.useController = false
        playerView.player = player

        val mediaSource = buildPlayerMediaSource(onboardingStreamingUrls, false)
        player.volume = 0f
        player.playWhenReady = playWhenReady
        player.repeatMode = Player.REPEAT_MODE_ALL
        player.videoScalingMode = C.VIDEO_SCALING_MODE_SCALE_TO_FIT
        player.seekTo(currentWindow, playbackPosition)
        player.addAnalyticsListener(EventLogger(trackSelector))
        player.prepare(mediaSource, false, false)

        return player
    }

    fun initUploadMediaPlayer(playerView: PlayerView, streamingUrls: AppStreamingUrls): SimpleExoPlayer {
        val trackSelector = DefaultTrackSelector()
        trackSelector.setParameters(trackSelector.buildUponParameters().setMaxVideoSizeSd())
        player = ExoPlayerFactory.newSimpleInstance(context, trackSelector)
        playerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM
        playerView.player = player

        val mediaSource = buildUriMediaSource(streamingUrls.localUri)
        player.playWhenReady = false
        player.repeatMode = Player.REPEAT_MODE_OFF
        player.videoScalingMode = C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING
        player.seekTo(currentWindow, playbackPosition)
        player.addAnalyticsListener(EventLogger(trackSelector))
        player.prepare(mediaSource, false, false)

        return player
    }

    fun initZoomPlayer(playerView: PlayerView, streamingUrls: AppStreamingUrls, loadProgressive: Boolean = false): SimpleExoPlayer {
        val trackSelector = DefaultTrackSelector()
        trackSelector.setParameters(trackSelector.buildUponParameters().setMaxVideoSizeSd())
        player = ExoPlayerFactory.newSimpleInstance(context, trackSelector)
        playerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIT
        playerView.player = player

        val mediaSource = buildPlayerMediaSource(streamingUrls, loadProgressive)
        player.playWhenReady = playWhenReady
        player.repeatMode = Player.REPEAT_MODE_OFF
        player.videoScalingMode = C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING
        player.seekTo(currentWindow, playbackPosition)
        player.addAnalyticsListener(EventLogger(trackSelector))
        player.prepare(mediaSource, false, false)

        return player
    }

    /**
     * first try to load dashStreamingUrl, on fallback
     * load hlsUrl, again on fallback
     * load smoothStreamingUrl, and at last on fallback
     * load progressiveUrl
     */
    private fun buildPlayerMediaSource(streamingUrls: AppStreamingUrls, loadProgressive: Boolean = false): MediaSource {
        val dashUrl = streamingUrls.dashUrl
        val hlsUrl = streamingUrls.hlsUrl
        val ssUrl = streamingUrls.ssUrl
        val asset = streamingUrls.asset
        val localUri = streamingUrls.localUri

        val progressiveCacheUrl = if(loadProgressive) streamingUrls.progressiveUrl?.cacheUrl() else streamingUrls.progressiveUrl

        return when {
            loadProgressive && progressiveCacheUrl.isNullOrBlank().not() -> {
                buildProgressiveMediaSource(Uri.parse(progressiveCacheUrl))
            }
            dashUrl.isNullOrBlank().not() -> {
                buildDashMediaSource(Uri.parse(dashUrl))
            }
            hlsUrl.isNullOrBlank().not() -> {
                buildHlsMediaSource(Uri.parse(hlsUrl))
            }
            ssUrl.isNullOrBlank().not() -> {
                buildSmoothStreamingMediaSource(Uri.parse(ssUrl))
            }
            asset.isNullOrBlank().not() -> {
                buildAssetsMediaSource(Uri.parse("assets:///$asset"))
            }
            localUri != null -> {
                buildUriMediaSource(localUri)
            }
            else -> {
                buildProgressiveMediaSource(null)
            }
        }
    }

    private fun buildAssetsMediaSource(uri: Uri?): MediaSource {
        val dataSourceFactory: DataSource.Factory = DataSource.Factory {
            AssetDataSource(context)
        }
        return ExtractorMediaSource(uri, dataSourceFactory, DefaultExtractorsFactory(), null, null)
    }

    private fun buildUriMediaSource(uri: Uri?): MediaSource {
        val dataSourceFactory = DefaultDataSourceFactory(context , context.resources.getString(R.string.app_name))
        val mediaSourceFactory = ExtractorMediaSource.Factory(dataSourceFactory)
        return mediaSourceFactory.createMediaSource(uri)
    }

    private fun buildProgressiveMediaSource(uri: Uri?): MediaSource {
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(context , context.resources.getString(R.string.app_name))
        val mediaSourceFactory = ProgressiveMediaSource.Factory(dataSourceFactory)
        return mediaSourceFactory.createMediaSource(uri)
    }

    private fun buildDashMediaSource(uri: Uri): MediaSource {
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(context , context.resources.getString(R.string.app_name))
        val mediaSourceFactory = DashMediaSource.Factory(dataSourceFactory)
        return mediaSourceFactory.createMediaSource(uri)
    }

    private fun buildHlsMediaSource(uri: Uri): MediaSource {
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(context , context.resources.getString(R.string.app_name))
        val mediaSourceFactory = HlsMediaSource.Factory(dataSourceFactory)
        return mediaSourceFactory.createMediaSource(uri)
    }

    private fun buildSmoothStreamingMediaSource(uri: Uri): MediaSource {
        val dataSourceFactory: DataSource.Factory = DefaultDataSourceFactory(context , context.resources.getString(R.string.app_name))
        val mediaSourceFactory = SsMediaSource.Factory(dataSourceFactory)
        return mediaSourceFactory.createMediaSource(uri)
    }

    fun releasePlayer() {
        playWhenReady = player.playWhenReady
        playbackPosition = player.currentPosition
        currentWindow = player.currentWindowIndex
        player.release()
    }
}