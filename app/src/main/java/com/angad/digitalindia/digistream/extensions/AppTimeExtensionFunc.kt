package com.angad.digitalindia.digistream.extensions

import com.angad.digitalindia.digistream.constants.AppGlobalConstants
import com.angad.digitalindia.digistream.constants.AppGlobalConstants.Companion.DAY_MILLIS
import com.angad.digitalindia.digistream.constants.AppGlobalConstants.Companion.HOUR_MILLIS
import com.angad.digitalindia.digistream.constants.AppGlobalConstants.Companion.MINUTE_MILLIS
import com.angad.digitalindia.digistream.constants.AppGlobalConstants.Companion.MONTH_MILLIS

/**
 * Created by Angad Tiwari on 11-07-2020.
 */
fun Long.formatInAgo(): String {
    val time = if (this < 1000000000000L) {
        this * 1000
    } else {
        this
    }
    val diff = System.currentTimeMillis() - time
    if (diff < MINUTE_MILLIS) {
        return "just now"
    } else if (diff < 2 * MINUTE_MILLIS) {
        return "a minute ago"
    } else if (diff < 50 * MINUTE_MILLIS) {
        return "${diff / MINUTE_MILLIS} minutes ago"
    } else if (diff < 90 * MINUTE_MILLIS) {
        return "an hour ago"
    } else if (diff < 24 * HOUR_MILLIS) {
        return "${diff / HOUR_MILLIS} hours ago"
    } else if (diff < 48 * HOUR_MILLIS) {
        return "yesterday";
    } else if((diff < 30 * DAY_MILLIS) || ((diff / MONTH_MILLIS) <= 0)) {
        return "${diff / DAY_MILLIS} days ago"
    } else {
        return "${diff / MONTH_MILLIS} months ago"
    }
}

fun Double.formatDuration(): String {
    if(this.toInt() == 0) {
        return "--:--"
    }
    return "${(this.toInt() / 60).toString().padStart(2, '0')}: ${(this.toInt() % 60).toString().padStart(2, '0')}"
}