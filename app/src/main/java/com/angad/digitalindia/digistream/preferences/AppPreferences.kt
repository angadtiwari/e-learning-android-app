package com.angad.digitalindia.digistream.preferences

import android.content.Context

/**
 * Created by Angad Tiwari on 07-07-2020.
 */
class AppPreferences(context: Context): AppPreferencesHelper(context) {
    var prefDeviceWidth by intPref()
    var prefDeviceHeight by intPref()

    var prefDeviceToken by stringPref() // fcm device token for push notification usage, required to send this to digi stream server

    var prefAppUser by userObjectPref() // app user object
    var prefCategories by categoriesObjectPref() // VoD, Streaming Video Categories
    var prefProfileTypes by profileTypesPref() // list of app profile types

    /**
     * clear all the preferences on signout
     * commented preference won't required to clear
     */
    fun clearPref() {
        prefAppUser = null
    }
}