package com.angad.digitalindia.digistream.notifications

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Angad Tiwari on 07-07-2020.
 */
@Parcelize
data class AppNotificationPayload(
    var id: String,
    var title: String,
    var body: String,
    val media: String?,
    val time: String?,
    val type: String? = "asset"
): Parcelable