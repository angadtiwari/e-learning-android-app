package com.angad.digitalindia.digistream.screens.dashboard

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.FragmentManager
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.base.AppBaseFragment
import com.angad.digitalindia.digistream.constants.AppEventsConstants
import com.angad.digitalindia.digistream.constants.AppRequestCodeConstants.IntentRequest.Companion.REQUEST_CAMERA
import com.angad.digitalindia.digistream.constants.AppSocialAccountConstants
import com.angad.digitalindia.digistream.extensions.fullscreen
import com.angad.digitalindia.digistream.extensions.normalscreen
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.models.eventbus.Events
import com.angad.digitalindia.digistream.networks.AppNetworkingService
import com.angad.digitalindia.digistream.screens.dashboard.nav.explore.ExploreHomeFragment
import com.angad.digitalindia.digistream.screens.dashboard.nav.golive.GoLiveHomeFragment
import com.angad.digitalindia.digistream.screens.dashboard.nav.notification.NotificationHomeFragment
import com.angad.digitalindia.digistream.screens.dashboard.nav.profile.ProfileHomeFragment
import com.angad.digitalindia.digistream.screens.dashboard.nav.upload.UploadHomeFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.MaterialShapeDrawable.SHADOW_COMPAT_MODE_ALWAYS
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.toolbar_app_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.greenrobot.eventbus.EventBus
import java.io.File


class DashboardActivity: AppBaseActivity() {

    private lateinit var mBuilder: NotificationCompat.Builder
    private var notificationId: Int = System.currentTimeMillis().toInt()

    private val exploreHomeFragment = ExploreHomeFragment()
    private val uploadHomeFragment = UploadHomeFragment()
    private val goLiveHomeFragment = GoLiveHomeFragment()
    private val notificationHomeFragment = NotificationHomeFragment()
    private val profileHomeFragment = ProfileHomeFragment()
    private var activeNavFragment: AppBaseFragment = exploreHomeFragment

    private var navViewSelectedId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        fetchUserSessionData()
        setupBottomNavigation()
        bottomNavigationShadow()
        initNavigationListener()
    }

    private fun fullscreenMode() {
        showToolbar = false
        nav_view_shadow.visibility = View.GONE
        nav_view.visibility = View.GONE
        fullscreen()
    }

    private fun normalscreenMode() {
        showToolbar = true
        nav_view_shadow.visibility = View.VISIBLE
        nav_view.visibility = View.VISIBLE
        normalscreen()
    }

    private fun fetchUserSessionData() {
        preference.prefAppUser?.let {
            if (it.provider != AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
                GlobalScope.launch(Dispatchers.IO) {
                    val response = AppNetworkingService.instance.userProfile()
                    if (response.isSuccessful) {
                        response.body()?.let {
                            preference.prefAppUser = it.data
                        }
                    }
                }
            }
        }
    }

    fun startUploadingProgressNotification() {
        // Create the NotificationChannel, but only on API 26+ because the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance: Int = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("CHANNEL_ID", "CHANNEL_NAME", importance)
            val notificationManager: NotificationManager = getSystemService(NotificationManager::class.java)!!
            notificationManager.createNotificationChannel(channel)
        }

        mBuilder = NotificationCompat.Builder(this, "CHANNEL_ID")
                .setSmallIcon(R.mipmap.ic_launcher_rounded)
                .setContentTitle("Digi Stream")
                .setContentText("Video Uploading is in progress...")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setProgress(100, 0, true)
                .setAutoCancel(true)
        val notificationManager = NotificationManagerCompat.from(this)
        notificationId = (System.currentTimeMillis() / 4).toInt()
        notificationManager.notify(notificationId, mBuilder.build())
    }

    private fun doneUploadingProgressNotification() {
        NotificationManagerCompat.from(this).apply {
            mBuilder.setContentText("Uploading completed.")
                .setProgress(100, 100, false)
            notify(notificationId, mBuilder.build())
        }
    }

    fun bottomNavigationShadow() {
        val background = nav_view.background
        if (background is MaterialShapeDrawable) {
            background.shadowCompatibilityMode = SHADOW_COMPAT_MODE_ALWAYS
        }
    }

    fun setupBottomNavigation() {
        preference.prefAppUser?.let {
            supportFragmentManager.beginTransaction().apply {
                add(R.id.frame_container, profileHomeFragment, ProfileHomeFragment::class.java.simpleName).hide(profileHomeFragment)
                if(it.provider != AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
                    add(R.id.frame_container, notificationHomeFragment, NotificationHomeFragment::class.java.simpleName).hide(notificationHomeFragment)
                }
                if(it.provider != AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
                    add(R.id.frame_container, goLiveHomeFragment, GoLiveHomeFragment::class.java.simpleName).hide(goLiveHomeFragment)
                }
                if(it.provider != AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
                    add(R.id.frame_container, uploadHomeFragment, UploadHomeFragment::class.java.simpleName).hide(uploadHomeFragment)
                }
                add(R.id.frame_container, exploreHomeFragment, ExploreHomeFragment::class.java.simpleName).show(exploreHomeFragment)
            }.commit()
            nav_view.selectedItemId = R.id.navigation_explore
            if(it.provider == AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
                nav_view.menu.findItem(R.id.navigation_upload).isEnabled = false
                nav_view.menu.findItem(R.id.navigation_golive).isEnabled = false
                nav_view.menu.findItem(R.id.navigation_notification).isEnabled = false
            }
        }
    }

    private fun initNavigationListener() {
        nav_view.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener{ item ->
            navViewSelectedId?.let {
                if(it == item.itemId) {
                    return@OnNavigationItemSelectedListener false
                }
            }
            navViewSelectedId = item.itemId
            when(item.itemId) {
                R.id.navigation_explore -> {
                    supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                    supportFragmentManager.beginTransaction().hide(activeNavFragment).show(exploreHomeFragment).commit()
                    activeNavFragment = exploreHomeFragment
                    txt_heading.text = item.title
                    normalscreenMode()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_upload -> {
                    supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                    supportFragmentManager.beginTransaction().hide(activeNavFragment).show(uploadHomeFragment).commit()
                    activeNavFragment = uploadHomeFragment
                    txt_heading.text = item.title
                    normalscreenMode()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_golive -> {
                    supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                    supportFragmentManager.beginTransaction().hide(activeNavFragment).show(goLiveHomeFragment).commit()
                    EventBus.getDefault().post(Events(AppEventsConstants.NEW_GOLIVE_SESSION))
                    activeNavFragment = goLiveHomeFragment
                    fullscreenMode()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_notification -> {
                    supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                    supportFragmentManager.beginTransaction().hide(activeNavFragment).show(notificationHomeFragment).commit()
                    activeNavFragment = notificationHomeFragment
                    txt_heading.text = item.title
                    normalscreenMode()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_profile -> {
                    supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                    supportFragmentManager.beginTransaction().hide(activeNavFragment).show(profileHomeFragment).commit()
                    activeNavFragment = profileHomeFragment
                    txt_heading.text = item.title
                    normalscreenMode()
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        })
    }

    override fun onMessageEvent(events: Events?) {
        when(events?.eventType) {
            AppEventsConstants.EVENT_UPLOADING_STARTED -> {
                events.bundle?.let {
                    startUploadingPlayback(it)
                    startUploadingProgressNotification()
                }
            }
            AppEventsConstants.EVENT_UPLOADING_DONE -> {
                doneUploadingProgressNotification()
            }
        }
    }

    private fun prepareTitleRequestBody(title: String?): RequestBody? {
        return if(title.isNullOrBlank().not()) {
            RequestBody.create(MultipartBody.FORM, title!!)
        } else {
            null
        }
    }

    private fun prepareCategoryRequestBody(category: String?): RequestBody? {
        return category?.let {
            RequestBody.create(MultipartBody.FORM, category!!)
        }
    }

    private fun prepareMultipartFileBody(path: String?): MultipartBody.Part? {
        val multipartBodyFile = File(path!!)
        val requestFile: RequestBody = RequestBody.create("video/*".toMediaTypeOrNull()!!, multipartBodyFile)

        return MultipartBody.Part.Companion.createFormData("playback", multipartBodyFile.name, requestFile)
    }

    private fun startUploadingPlayback(bundle: Bundle) {
        val requestTitleBody = prepareTitleRequestBody(bundle.getString("title"))!!
        val requestCategoryBody = prepareCategoryRequestBody(bundle.getString("category"))!!
        val multipartBody = prepareMultipartFileBody(bundle.getString("playback"))!!

        val map = mutableMapOf<String, RequestBody>(
            "title" to requestTitleBody,
            "category" to requestCategoryBody
        )

        GlobalScope.launch(Dispatchers.IO) {
            val response = AppNetworkingService.instance.upload(map, multipartBody)
            GlobalScope.launch(Dispatchers.Main) {
                if (response.isSuccessful && response.body() != null) {
                    EventBus.getDefault().post(Events(AppEventsConstants.EVENT_UPLOADING_DONE))
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val bundle = Bundle()
                bundle.putBoolean("granted", true)
                EventBus.getDefault().post(Events(AppEventsConstants.EVENT_CAMERA_PERMISSION_GRANTED, bundle))
            } else {
                val bundle = Bundle()
                bundle.putBoolean("granted", false)
                EventBus.getDefault().post(Events(AppEventsConstants.EVENT_CAMERA_PERMISSION_GRANTED, bundle))
            }
        }
    }

    override fun onBackPressed() {
        if(activeNavFragment !is ExploreHomeFragment) {
            nav_view.selectedItemId = R.id.navigation_explore
        } else {
            super.onBackPressed()
        }
    }
}