package com.angad.digitalindia.digistream.callbacks

/**
 * Created by Angad Tiwari on 11-07-2020.
 */
interface IOnGenericCallback {
    fun onSuccess()
    fun onFailure()
}