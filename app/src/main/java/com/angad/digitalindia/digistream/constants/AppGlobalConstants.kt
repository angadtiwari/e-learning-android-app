package com.angad.digitalindia.digistream.constants

/**
 * Created by Angad Tiwari on 11-07-2020.
 */
class AppGlobalConstants {
    companion object {
        const val ONBOARDING_BGVIDEO_NAME = "digistream_onboarding.mp4"

        const val PAGING_SIZE = 10

        const val SECOND_MILLIS = 1000
        const val MINUTE_MILLIS = 60 * SECOND_MILLIS
        const val HOUR_MILLIS = 60 * MINUTE_MILLIS
        const val DAY_MILLIS = 24 * HOUR_MILLIS
        const val MONTH_MILLIS = 30 * DAY_MILLIS
    }
}