package com.angad.digitalindia.digistream.extensions

import com.facebook.drawee.generic.RoundingParams
import com.facebook.drawee.view.SimpleDraweeView
import kotlinx.android.synthetic.main.fragment_profile_home.*

/**
 * Created by Angad Tiwari on 10-07-2020.
 */
fun SimpleDraweeView.makeRounded() {
    val color = resources.getColor(android.R.color.white)
    val roundingParams = RoundingParams.fromCornersRadius(5f)
    roundingParams.setBorder(color, 1.0f)
    roundingParams.roundAsCircle = true
    this.hierarchy.roundingParams = roundingParams
}

inline fun String.defaultPlaceholderProfilePic(): String =
    "https://ui-avatars.com/api/?name=$this"

inline fun String.facebookProfilePic(): String =
    "https://graph.facebook.com/${this}/picture?width=720&width=720"