package com.angad.digitalindia.digistream.constants

/**
 * Created by Angad Tiwari on 07-07-2020.
 */
class AppRequestCodeConstants {

    class IntentRequest {
        companion object {
            const val REQUEST_GOOGLE_SIGNIN = 101
            const val REQUEST_CHOOSE_VIDEO = 102
            const val REQUEST_CAMERA = 103
        }
    }
}