package com.angad.digitalindia.digistream.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagedList
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.toLiveData
import com.angad.digitalindia.digistream.db.dao.NotificationDao
import com.angad.digitalindia.digistream.models.Notification
import com.angad.digitalindia.digistream.repositories.NotificationRepository
import kotlinx.coroutines.flow.Flow

/**
 * Created by Angad Tiwari on 11-07-2020.
 */
class NotificationViewModel(val notificationRepository: NotificationRepository, val notificationDao: NotificationDao? = null): ViewModel() {
    private var currentNotificationResult: Flow<PagingData<Notification>>? = null

    fun getNotifications(): Flow<PagingData<Notification>> {
        val newResult: Flow<PagingData<Notification>> = notificationRepository.getNotifications()
            .cachedIn(viewModelScope)
        currentNotificationResult = newResult
        return newResult
    }
}