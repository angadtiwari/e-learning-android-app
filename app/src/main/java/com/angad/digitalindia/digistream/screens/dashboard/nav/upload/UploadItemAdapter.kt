package com.angad.digitalindia.digistream.screens.dashboard.nav.upload

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.extensions.*
import com.angad.digitalindia.digistream.models.Playback
import com.angad.digitalindia.digistream.screens.common.zoomview.ZoomVideoActivity
import kotlinx.android.synthetic.main.adapter_upload_item.view.*
import kotlinx.android.synthetic.main.adapter_upload_item.view.imageView2
import kotlinx.android.synthetic.main.adapter_upload_item.view.txt_explore_item_category
import kotlinx.android.synthetic.main.adapter_upload_item.view.txt_explore_item_title
import kotlinx.android.synthetic.main.adapter_upload_item.view.txt_explore_item_uploadedat
import java.util.ArrayList

/**
 * Created by Angad Tiwari on 08-07-2020.
 */
class UploadItemAdapter(val activity: FragmentActivity, diffCallback: DiffUtil.ItemCallback<Playback>): PagingDataAdapter<Playback, UploadItemAdapter.ViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(this,
            LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_upload_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val upload = getItem(position)
        upload?.let {
            holder.bind(upload)
        }
    }

    class ViewHolder(val uploadItemAdapter: UploadItemAdapter, itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Playback) = with(itemView) {
            txt_explore_item_title.text = item.title
            if(item.type == "asset") {
                img_livestreaming_blink.visibility = View.GONE
                imageView2.setImageURI(item.playback_ids[0].id.playbackThumbnailUrl())
            } else {
                img_livestreaming_blink.visibility = View.VISIBLE
                if(item.status != "completed") {
                    img_livestreaming_blink.blinkLiveStreaming()
                }
                imageView2.setImageURI(item.playback_ids[0].id.liveStreamingDefaultThumbnailUrl())
            }
            txt_explore_item_uploadedat.text = item.created_at.formatInAgo()
            txt_explore_item_timelength.text = item.duration.formatDuration()
            txt_explore_item_category.text = item.category.name

            setOnClickListener {
                val intent = Intent(uploadItemAdapter.activity, ZoomVideoActivity::class.java)
                intent.putExtra("playbackId", item.playback_ids[0].id)
                intent.putExtra("livestream", (item.type == "livestream"))
                uploadItemAdapter.activity.startActivity(intent)
            }
        }
    }
}