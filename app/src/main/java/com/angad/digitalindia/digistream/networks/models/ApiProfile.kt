package com.angad.digitalindia.digistream.networks.models

import com.angad.digitalindia.digistream.constants.AppSocialAccountConstants
import com.angad.digitalindia.digistream.models.Category
import com.angad.digitalindia.digistream.models.ProfileType
import com.angad.digitalindia.digistream.models.User

/**
 * Created by Angad Tiwari on 10-07-2020.
 */
class ApiProfile {
    data class Request(val organization_name: String? = null, val type: ProfileType? = null, val categories: MutableList<Category>)
    data class Response(val code: Int, val status: String, val message: String, val data: User.Profile)

    class ApiLogin {
        data class Request(
            val _id: String? = null,
            var id: String? = null,
            var first_name: String? = AppSocialAccountConstants.ProviderName.PROVIDER_GUEST,
            var last_name: String? = null,
            val email: String,
            val birthday: String? = null,
            val gender: String? = null,
            val link: String? = null,
            val locale: String? = null,
            val location: User.Location? = null,
            var name: String? = AppSocialAccountConstants.ProviderName.PROVIDER_GUEST,
            val timezone: Int? = null,
            val updated_time: String? = null,
            val verified: Boolean? = null,
            var provider: String? = AppSocialAccountConstants.ProviderName.PROVIDER_GUEST,
            var profile_pic: String? = null,
            var social_account_token: String? = null,
            var access_token: String? = null,
            var push_token: String? = null)
    }

    class ApiCategory {
        data class Response(val code: Int, val status: String, val message: String, val data: List<Category>)
    }

    class ApiType {
        data class Response(val code: Int, val status: String, val message: String, val data: List<ProfileType>)
    }
}