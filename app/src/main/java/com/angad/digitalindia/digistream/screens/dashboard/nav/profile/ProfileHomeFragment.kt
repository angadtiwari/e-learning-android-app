package com.angad.digitalindia.digistream.screens.dashboard.nav.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.base.AppBaseFragment
import com.angad.digitalindia.digistream.components.AppDialogView
import com.angad.digitalindia.digistream.constants.AppEnvironmentConstants
import com.angad.digitalindia.digistream.constants.AppEventsConstants
import com.angad.digitalindia.digistream.constants.AppSocialAccountConstants
import com.angad.digitalindia.digistream.extensions.*
import com.angad.digitalindia.digistream.models.Category
import com.angad.digitalindia.digistream.models.eventbus.Events
import com.angad.digitalindia.digistream.networks.AppNetworkingService
import com.angad.digitalindia.digistream.networks.models.ApiProfile
import com.angad.digitalindia.digistream.screens.common.WebViewActivity
import com.angad.digitalindia.digistream.screens.common.zoomview.ZoomPhotoActivity
import com.angad.digitalindia.digistream.screens.dashboard.DashboardActivity
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipDrawable
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.fragment_profile_home.*
import kotlinx.android.synthetic.main.toolbar_app_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus


/**
 * Created by Angad Tiwari on 08-07-2020.
 */
class ProfileHomeFragment: AppBaseFragment() {

    private var userSelectedCategories: MutableList<Category> = mutableListOf()
    private var userSelectedCategoryIds: MutableList<String> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as DashboardActivity).txt_heading.text = resources.getString(R.string.title_profile)

        initView()
        initListeners()
        initCategoryChips()
    }

    private fun initView() {
        context?.preference?.prefAppUser?.let {
            if(it.profile_pic.isNullOrBlank()) {
                img_profile_pic.setImageURI(it.name?.defaultPlaceholderProfilePic())
            } else {
                img_profile_pic.setImageURI(it.profile_pic)
            }
            if(it.provider == AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
                btn_logout.setText("Login")
                chip_profile_type.visibility = View.VISIBLE
                chip_profile_type.text = AppSocialAccountConstants.ProviderName.PROVIDER_GUEST.toUpperCase()
            }
            txt_profile_name.text = it.name
            txt_profile_email.text = it.email
            if(it.organization_name.isNullOrBlank().not()) {
                txt_profile_organization.visibility = View.VISIBLE
                txt_profile_organization.text = it.organization_name
            }
            it.type?.let {type ->
                chip_profile_type.visibility = View.VISIBLE
                chip_profile_type.text = type.type.toUpperCase()
            }
        }
        img_profile_pic.makeRounded()
    }

    private fun initCategoryChips() {
        userSelectedCategories = context?.preference?.prefAppUser?.categories?.filter { it.selected }?.toMutableList() ?: mutableListOf()
        userSelectedCategoryIds = userSelectedCategories.map { it._id }.toMutableList()
        context?.preference?.prefCategories?.let {
            it.forEach {category ->
                addCategoryChip(chipGroup, category)
            }
        }
    }

    private fun addCategoryChip(chipGroup: ChipGroup, category: Category) {
        val chip = Chip(context)
        chip.setChipDrawable(ChipDrawable.createFromResource(requireContext(), R.xml.chip_category))
        chip.text = category.name
        chip.isChecked = userSelectedCategoryIds.contains(category._id)
        chip.setOnCheckedChangeListener { compoundButton, b ->
            category.selected = b
            if(b) {
                userSelectedCategories.add(category)
            } else {
                val categoryIndexToRemove = userSelectedCategories.indexOfFirst { it._id == category._id }
                userSelectedCategories.removeAt(categoryIndexToRemove)
            }

            if(context?.preference?.prefAppUser?.provider == AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
                return@setOnCheckedChangeListener
            }

            val profileRequest = ApiProfile.Request(categories = userSelectedCategories)
            GlobalScope.launch(Dispatchers.IO) {
                val response = AppNetworkingService.instance.updateProfile(profileRequest)
                if(response.isSuccessful) {
                    response.body()?.let {
                        requireContext().preference.prefAppUser = it.data
                    }
                }
            }
        }
        chipGroup.addView(chip)
    }

    private fun initListeners() {
        btn_logout.setOnClickListener {
            context?.preference?.prefAppUser?.let { profile ->
                if (profile.provider != AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
                    appFragmentDialog(resources.getString(R.string.app_name), "Sure you want to logout ?", AppDialogView.INFO, false, true) {
                        clearLoginSession()
                    }
                } else {
                    clearLoginSession()
                }
            }
        }
        img_profile_pic.setOnClickListener {
            val profilePic = if(context?.preference?.prefAppUser?.profile_pic.isNullOrBlank()) {
                "https://ui-avatars.com/api/?name=${it.context?.preference?.prefAppUser?.name}"
            } else {
                context?.preference?.prefAppUser?.profile_pic
            }
            val intent = Intent(context, ZoomPhotoActivity::class.java)
            intent.putExtra("photo_url", profilePic)
            context?.startActivity(intent)
        }

        txt_privacy_policy.setOnClickListener {
            val intent = Intent(activity, WebViewActivity::class.java)
            intent.putExtra("title", "Privacy Policy")
            intent.putExtra("url", AppEnvironmentConstants.WebPageUrls.PAGE_PRIVACY_POLICY)
            startActivity(intent)
            activity?.overridePendingTransition(0, android.R.anim.fade_out)
        }
        txt_terms_n_condition.setOnClickListener {
            val intent = Intent(activity, WebViewActivity::class.java)
            intent.putExtra("title", "Terms & Conditions")
            intent.putExtra("url", AppEnvironmentConstants.WebPageUrls.PAGE_TERMS_CONDITION)
            startActivity(intent)
            activity?.overridePendingTransition(0, android.R.anim.fade_out)
        }
    }

    override fun onMessageEvent(events: Events) {
        super.onMessageEvent(events)

        when (events.eventType) {

        }
    }
}