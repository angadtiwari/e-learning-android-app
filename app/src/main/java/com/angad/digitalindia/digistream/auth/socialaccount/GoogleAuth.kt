package com.angad.digitalindia.digistream.auth.socialaccount

import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.fragment.app.FragmentActivity
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.auth.IAppLoginAuthentication
import com.angad.digitalindia.digistream.auth.IOnAuthCallback
import com.angad.digitalindia.digistream.constants.AppRequestCodeConstants.IntentRequest.Companion.REQUEST_GOOGLE_SIGNIN
import com.angad.digitalindia.digistream.constants.AppSocialAccountConstants
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.models.User
import com.angad.digitalindia.digistream.screens.onboarding.OnboardingActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task


/**
 * Created by Angad Tiwari on 07-07-2020.
 */
class GoogleAuth: IAppLoginAuthentication {

    private var mGoogleSignInClient: GoogleSignInClient? = null

    override fun loginRequest(activity: FragmentActivity) {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(activity.getString(R.string.google_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(activity, gso)
        val signInIntent = mGoogleSignInClient?.signInIntent
        activity.startActivityForResult(signInIntent, REQUEST_GOOGLE_SIGNIN)
    }

    override fun login(activity: FragmentActivity, iOnAuthCallback: IOnAuthCallback?, data: Intent?) {
        val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
        try {
            val account: GoogleSignInAccount? = task.getResult(ApiException::class.java)
            Log.w(OnboardingActivity::class.java.simpleName, "signInResult")
            val acct = GoogleSignIn.getLastSignedInAccount(activity)
            if (acct != null) {
                val personName = acct.displayName
                val personGivenName = acct.givenName
                val personFamilyName = acct.familyName
                val personEmail = acct.email
                val personId = acct.id
                val personPhoto: String? = acct.photoUrl.toString().replace("s96-c", "s720-c", true)
                val accessToken = acct.idToken

                personEmail?.let {
                    val userProfile = User.Profile(email = personEmail, push_token = activity.preference.prefDeviceToken)
                    userProfile.first_name = personGivenName
                    userProfile.last_name = personFamilyName
                    userProfile.name = personName
                    userProfile.id = personId
                    userProfile.social_account_token = accessToken
                    userProfile.provider = AppSocialAccountConstants.ProviderName.PROVIDER_GOOGLE
                    userProfile.profile_pic = personPhoto
                    activity.preference.prefAppUser = userProfile
                    iOnAuthCallback?.onSuccess(userProfile)
                } ?: iOnAuthCallback?.onFailure("failed to login with google")
            }
        } catch (e: ApiException) {
            Log.w(OnboardingActivity::class.java.simpleName, "signInResult:failed code=" + e.statusCode)
            e.printStackTrace()
            iOnAuthCallback?.onFailure("failed to login with google")
        }
    }

    override fun logout() {
        mGoogleSignInClient?.signOut()?.addOnCompleteListener {

        }
    }

    companion object {
        val instance: GoogleAuth =
            GoogleAuth()
    }
}