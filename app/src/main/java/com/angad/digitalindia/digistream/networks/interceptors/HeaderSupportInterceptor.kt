package com.angad.digitalindia.digistream.networks.interceptors

import android.content.Context
import android.content.Intent
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.screens.onboarding.OnboardingActivity
import okhttp3.*
import org.greenrobot.eventbus.EventBus

class HeaderSupportInterceptor(val context: Context): Interceptor, Authenticator {

    /**
     * Interceptor class for setting of the headers for every request
     */
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val requestBuilder = request.newBuilder()
        requestBuilder.addHeader("Content-Type", "application/json")
        requestBuilder.addHeader("Accept", "application/json")
        context.preference.prefAppUser?.let {
            it.access_token?.let { accessToken ->
                requestBuilder.addHeader("Authorization", accessToken)
            }
        }
        request = requestBuilder.build()
        val response = chain.proceed(request)

        if(response.code == 401) {
            context.preference.clearPref()
            val intent = Intent(context, OnboardingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
        return response
    }

    /**
     * Authenticator for when the authToken need to be refresh and updated
     * everytime we get a 401 error code
     */
    override fun authenticate(route: Route?, response401: Response): Request? {
        val requestAvailable: Request? = null
        return requestAvailable
    }
}