package com.angad.digitalindia.digistream.screens.dashboard.nav.explore

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.extensions.*
import com.angad.digitalindia.digistream.models.Playback
import com.angad.digitalindia.digistream.screens.common.zoomview.ZoomVideoActivity
import kotlinx.android.synthetic.main.adapter_explore_item.view.*
import java.util.*

/**
 * Created by Angad Tiwari on 08-07-2020.
 */
class ExploreItemAdapter(val activity: FragmentActivity, diffCallback: DiffUtil.ItemCallback<Playback>): PagingDataAdapter<Playback, ExploreItemAdapter.ViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(this,
            LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_explore_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ExploreItemAdapter.ViewHolder, position: Int) {
        val playback= getItem(position)
        playback?.let {
            holder.bind(it)
        }
    }

    class ViewHolder(val exploreItemAdapter: ExploreItemAdapter, itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Playback) = with(itemView) {
            txt_explore_item_title.text = item.title
            if(item.type == "asset") {
                img_livestreaming_blink.visibility = View.GONE
                imageView2.setImageURI(item.playback_ids[0].id.playbackThumbnailUrl())
            } else {
                img_livestreaming_blink.visibility = View.VISIBLE
                img_livestreaming_blink.blinkLiveStreaming()
                imageView2.setImageURI(item.playback_ids[0].id.liveStreamingDefaultThumbnailUrl())
            }
            txt_explore_item_uploadedat.text = item.created_at.formatInAgo()
            txt_explore_item_timelength.text = item.duration.formatDuration()
            txt_explore_item_category.text = item.category.name
            txt_explore_item_organisation.text = if(item.uploader.organization_name.isNullOrBlank()) item.uploader.name else item.uploader.organization_name

            setOnClickListener {
                val intent = Intent(exploreItemAdapter.activity, ZoomVideoActivity::class.java)
                intent.putExtra("playbackId", item.playback_ids[0].id)
                intent.putExtra("livestream", (item.type == "livestream"))
                exploreItemAdapter.activity.startActivity(intent)
            }
        }
    }
}