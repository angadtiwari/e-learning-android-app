package com.angad.digitalindia.digistream.callbacks

import android.graphics.Bitmap

/**
 * Created by Angad Tiwari on 15-06-2020.
 */
interface IOnPushBigPicture {
    fun onSuccess(bitmap: Bitmap)
    fun onFailure()
}