package com.angad.digitalindia.digistream.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Angad Tiwari on 08-07-2020.
 */
@Parcelize
data class Playback(
    val _id: String,
    val created_at: Long,
    val id: String,
    val stream_key: String,
    val reconnect_window: Int,
    val playback_ids: List<PlaybackId>,
    val status: String,
    val type: String,
    val uploader: User.Profile,
    val title: String,
    val category: Category,
    val duration: Double
): Parcelable

@Parcelize
data class PlaybackId(
    val id: String,
    val policy: String
): Parcelable