package com.angad.digitalindia.digistream.extensions

import android.app.Activity
import android.util.DisplayMetrics

/**
 * Created by Angad Tiwari on 15-07-2020.
 */
fun Activity.computeDeviceDimension() {
    val displayMetrics = DisplayMetrics()
    this.windowManager.defaultDisplay.getMetrics(displayMetrics)
    val width = displayMetrics.widthPixels
    val height = displayMetrics.heightPixels

    this.preference.prefDeviceWidth = width
    this.preference.prefDeviceHeight = height
}