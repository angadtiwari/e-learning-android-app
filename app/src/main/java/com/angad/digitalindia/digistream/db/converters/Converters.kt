package com.angad.digitalindia.digistream.db.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

/**
 * Created by Angad Tiwari on 11-07-2020.
 */
class Converters {

    /**
     * converters for list<string> for db
     */
    @TypeConverter
    fun listToString(list: List<String?>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun stringToList(value: String?): List<String> {
        val listType: Type = object : TypeToken<ArrayList<String?>?>() {}.type
        return Gson().fromJson(value, listType)?: listOf()
    }

    /**
     * converters for list<int> for db
     */
    @TypeConverter
    fun listToInt(list: List<Int?>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }

    @TypeConverter
    fun intToList(value: String?): List<Int> {
        val listType: Type = object : TypeToken<ArrayList<Int?>?>() {}.type
        return Gson().fromJson(value, listType)
    }
}