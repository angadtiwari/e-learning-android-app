package com.angad.digitalindia.digistream.auth.socialaccount

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.angad.digitalindia.digistream.auth.IAppLoginAuthentication
import com.angad.digitalindia.digistream.auth.IOnAuthCallback
import com.angad.digitalindia.digistream.constants.AppSocialAccountConstants
import com.angad.digitalindia.digistream.extensions.facebookProfilePic
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.models.User
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.gson.Gson
import java.lang.Exception


/**
 * Created by Angad Tiwari on 07-07-2020.
 */
class FacebookAuth: IAppLoginAuthentication {

    var callbackManager: CallbackManager? = null

    override fun loginRequest(activity: FragmentActivity) {

    }

    override fun login(activity: FragmentActivity, iOnAuthCallback: IOnAuthCallback?, intent: Intent?) {
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().registerCallback(callbackManager,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) {
                    loginResult?.accessToken?.let {accessToken ->
                        val request = GraphRequest.newMeRequest(accessToken) { `object`, _ ->
                            try {
                                val userProfile = Gson().fromJson(`object`.toString(), User.Profile::class.java)
                                userProfile.social_account_token = accessToken.token
                                userProfile.provider = AppSocialAccountConstants.ProviderName.PROVIDER_FACEBOOK
                                userProfile.profile_pic = userProfile.id?.facebookProfilePic()//`object`.getJSONObject("picture").getJSONObject("data").getString("url")
                                userProfile.push_token = activity.preference.prefDeviceToken
                                activity.preference.prefAppUser = userProfile
                                iOnAuthCallback?.onSuccess(userProfile)
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                                iOnAuthCallback?.onFailure("failed to login with facebook")
                            }
                        }
                        val parameters = Bundle()
                        parameters.putString("fields", "id, first_name, last_name, name, gender, birthday, email, link, verified, updated_time, timezone, locale, location, picture.type(large)")
                        request.parameters = parameters
                        request.executeAsync()
                    }
                }

                override fun onCancel() {
                    iOnAuthCallback?.onFailure("failed to login with facebook")
                }

                override fun onError(exception: FacebookException) {
                    iOnAuthCallback?.onFailure("failed to login with facebook")
                }
            })
        LoginManager.getInstance().logInWithReadPermissions(activity, listOf("public_profile", "email"))
    }

    override fun logout() {
        val request = GraphRequest(
            AccessToken.getCurrentAccessToken(),
            "/me/permissions/",
            null,
            HttpMethod.DELETE,
            GraphRequest.Callback {
                // Insert your code here
                LoginManager.getInstance().logOut()
            })

        request.executeAsync()
    }

    companion object {
        val instance: FacebookAuth =
            FacebookAuth()
    }
}