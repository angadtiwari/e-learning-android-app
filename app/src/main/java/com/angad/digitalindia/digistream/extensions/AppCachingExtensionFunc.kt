package com.angad.digitalindia.digistream.extensions

import android.util.Log
import com.angad.digitalindia.digistream.base.AppDelegate
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.net.URL
import java.util.concurrent.Executors

/**
 * Created by Angad Tiwari on 07-07-2020.
 */

fun String.cacheUrl(): String {
    val cacheUrl = AppDelegate.instance().initHttpProxyCacheServer().getProxyUrl(this)
    return if(cacheUrl.isNullOrBlank()) this else cacheUrl
}

fun String.cacheUrlAvailable(): Boolean {
    return AppDelegate.instance().initHttpProxyCacheServer().isCached(this)
}

fun String.startCachingUrl() {
    Executors.newSingleThreadExecutor().submit {
        AppDelegate.instance().initHttpProxyCacheServer().registerCacheListener({ cacheFile: File?, url: String?, percentsAvailable: Int ->
            Log.d("startCachingUrl", "$percentsAvailable% available for url $url")
        }, this)
        var inputStream: InputStream? = null
        try {
            val proxyUrl = AppDelegate.instance().initHttpProxyCacheServer().getProxyUrl(this)
            val url = URL(proxyUrl)
            inputStream = url.openStream()
            //val byteChunk: ByteArray = IOUtils.toByteArray(inputStream)
        } catch (e: Exception) {
            Log.d("startCachingUrl", "Failed while reading bytes from $this: ${e.message}")
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close()
                } catch (e: IOException) {
                    // Long hair, don't care.
                    e.printStackTrace()
                }
            }
        }
    }
}