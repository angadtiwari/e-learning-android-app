package com.angad.digitalindia.digistream.screens.dashboard.nav.notification

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.extensions.formatInAgo
import com.angad.digitalindia.digistream.models.Notification
import com.angad.digitalindia.digistream.screens.common.zoomview.ZoomVideoActivity
import kotlinx.android.synthetic.main.adapter_notification_item.view.*
import java.util.ArrayList

/**
 * Created by Angad Tiwari on 08-07-2020.
 */
class NotificationItemAdapter(val activity: FragmentActivity, diffCallback: DiffUtil.ItemCallback<Notification>): PagingDataAdapter<Notification, NotificationItemAdapter.ViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(this,
            LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_notification_item, parent, false)
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val notifications= getItem(position)
        notifications?.let {
            holder.bind(it)
        }
    }

    class ViewHolder(val notificationItemAdapter: NotificationItemAdapter, itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Notification) = with(itemView) {
            itemView.txt_notification_item_title.text = "${item.title} - ${item.body}"
            itemView.txt_notification_item_uploadedat.text = item.time.formatInAgo()
            itemView.imageView2.setImageURI(item.media)

            setOnClickListener {
                item.playback?.playback_ids[0]?.id?.let {
                    val intent = Intent(notificationItemAdapter.activity, ZoomVideoActivity::class.java)
                    intent.putExtra("playbackId", it)
                    intent.putExtra("livestream", (item.playback.type == "livestream"))
                    notificationItemAdapter.activity.startActivity(intent)
                }
            }
        }
    }
}