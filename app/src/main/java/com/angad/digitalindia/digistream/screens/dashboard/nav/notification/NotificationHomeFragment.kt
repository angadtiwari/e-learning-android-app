package com.angad.digitalindia.digistream.screens.dashboard.nav.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.base.AppBaseFragment
import com.angad.digitalindia.digistream.extensions.normalscreen
import com.angad.digitalindia.digistream.injections.Injection
import com.angad.digitalindia.digistream.models.Notification
import com.angad.digitalindia.digistream.models.eventbus.Events
import com.angad.digitalindia.digistream.screens.dashboard.DashboardActivity
import com.angad.digitalindia.digistream.viewmodel.NotificationViewModel
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.fragment_explore_home.*
import kotlinx.android.synthetic.main.fragment_notification_home.*
import kotlinx.android.synthetic.main.toolbar_app_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class NotificationHomeFragment: AppBaseFragment() {

    private lateinit var viewModel: NotificationViewModel
    private lateinit var adapterNotification: NotificationItemAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_notification_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as DashboardActivity).txt_heading.text = resources.getString(R.string.title_notification)

        initNotificationRecyler()
        fetchNotificationList()
        initListeners()
    }

    private fun fetchNotificationList() {
        progress = if(swiperefresh_notification.isRefreshing.not()) View.VISIBLE else View.GONE
        lifecycleScope.launch {
            viewModel.getNotifications().collectLatest { pagingData ->
                GlobalScope.launch(Dispatchers.IO) {
                    //TODO: need to insert notification paging data into room db
                }
                adapterNotification.submitData(pagingData)
            }
        }
    }

    private fun initNotificationRecyler() {
        viewModel = ViewModelProvider(this, Injection.provideNotificationViewModelFactory()).get(NotificationViewModel::class.java)
        adapterNotification = NotificationItemAdapter(requireActivity(), object: DiffUtil.ItemCallback<Notification>() {
            override fun areItemsTheSame(oldItem: Notification, newItem: Notification): Boolean {
                return oldItem._id == newItem._id
            }

            override fun areContentsTheSame(oldItem: Notification, newItem: Notification): Boolean {
                return oldItem._id == newItem._id
            }
        })
        recycler_notification.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recycler_notification.adapter = adapterNotification

        GlobalScope.launch(Dispatchers.Main) {
            adapterNotification.loadStateFlow.collectLatest { loadStates ->
                progress = if (loadStates.refresh is LoadState.Loading && swiperefresh_notification.isRefreshing.not()) View.VISIBLE else View.GONE
                layout_notification_emptyview?.visibility = if(loadStates.refresh !is LoadState.Loading && adapterNotification.itemCount == 0) View.VISIBLE else View.GONE
                swiperefresh_notification?.isRefreshing = loadStates.refresh is LoadState.Loading && progress != View.VISIBLE
            }
        }
    }

    private fun initListeners() {
        swiperefresh_notification.setOnRefreshListener {
            fetchNotificationList()
        }
    }

    override fun onMessageEvent(events: Events) {
        super.onMessageEvent(events)

        when (events.eventType) {

        }
    }
}