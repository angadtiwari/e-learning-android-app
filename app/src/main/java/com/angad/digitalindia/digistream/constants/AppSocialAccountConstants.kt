package com.angad.digitalindia.digistream.constants

/**
 * Created by Angad Tiwari on 07-07-2020.
 */
class AppSocialAccountConstants {

    class ProviderName {
        companion object {
            const val PROVIDER_GOOGLE = "google"
            const val PROVIDER_FACEBOOK = "facebook"
            const val PROVIDER_GUEST = "guest"
        }
    }

    class Keys {
        companion object {
            const val GOOGLE_CLIENT_ID = "978031542858-4gh6l1e49ab9mpf0mgduemevf93c4qq8.apps.googleusercontent.com"
        }
    }
}