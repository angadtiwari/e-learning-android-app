package com.angad.digitalindia.digistream.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.angad.digitalindia.digistream.db.converters.Converters
import com.angad.digitalindia.digistream.db.dao.NotificationDao
import com.angad.digitalindia.digistream.models.Notification

/**
 * Created by Angad Tiwari on 11-07-2020.
 */
@Database(entities = [Notification::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppRoomDatabase: RoomDatabase() {
    abstract fun notificationDao(): NotificationDao
}