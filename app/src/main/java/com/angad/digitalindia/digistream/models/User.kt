package com.angad.digitalindia.digistream.models

import android.os.Parcelable
import com.angad.digitalindia.digistream.constants.AppSocialAccountConstants
import kotlinx.android.parcel.Parcelize

/**
 * Created by Angad Tiwari on 07-07-2020.
 */
class User {

    @Parcelize
    data class Profile(
        val _id: String? = null,
        var id: String? = null,
        var first_name: String? = AppSocialAccountConstants.ProviderName.PROVIDER_GUEST,
        var last_name: String? = null,
        val email: String,
        val birthday: String? = null,
        val gender: String? = null,
        val link: String? = null,
        val locale: String? = null,
        val location: Location? = null,
        var name: String? = AppSocialAccountConstants.ProviderName.PROVIDER_GUEST,
        val timezone: Int? = null,
        val updated_time: String? = null,
        val verified: Boolean? = null,
        var provider: String? = AppSocialAccountConstants.ProviderName.PROVIDER_GUEST,
        var profile_pic: String? = null,
        var social_account_token: String? = null,
        var access_token: String? = null,
        var categories: MutableList<Category>? = null,
        var type: ProfileType? = null,
        var organization_name: String? = null,
        var push_token: String? = null
    ): Parcelable

    @Parcelize
    data class Location(
        val id: String,
        val name: String
    ): Parcelable
}