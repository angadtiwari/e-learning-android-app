package com.angad.digitalindia.digistream.base

import android.app.Application
import androidx.room.Room
import com.angad.digitalindia.digistream.constants.AppDatabaseConstants.Companion.DB_NAME
import com.angad.digitalindia.digistream.db.AppRoomDatabase
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.socket.AppSocketManager
import com.danikula.videocache.HttpProxyCacheServer
import com.facebook.FacebookSdk
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.firebase.FirebaseApp

/**
 * Created by Angad Tiwari on 05-07-2020.
 */
class AppDelegate: Application() {

    init {
        instance = this
    }

    companion object {
        private var dbInstance: AppRoomDatabase? = null
        private var instance: AppDelegate? = null
        private var httpProxyCacheServer: HttpProxyCacheServer? = null

        fun instance(): AppDelegate = instance!!

        fun roomDatabase(): AppRoomDatabase {
            if (dbInstance != null) {
                return dbInstance as AppRoomDatabase
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(instance(), AppRoomDatabase::class.java, DB_NAME)
                    .build()
                dbInstance = instance
                return instance
            }
        }
    }

    override fun onCreate() {
        super.onCreate()

        initHttpProxyCacheServer()
        initFirebase()
        initSocket()
        initFresco()
    }

    private fun initFresco() {
        Fresco.initialize(applicationContext)
    }

    private fun initSocket() {
        AppSocketManager.connectSocket()?.let {
            AppSocketManager.listenEvent()
        }
    }

    private fun initFirebase() {
        FirebaseApp.initializeApp(this)
    }

    fun initHttpProxyCacheServer(): HttpProxyCacheServer {
        if(httpProxyCacheServer == null) {
            httpProxyCacheServer = HttpProxyCacheServer(this.applicationContext)
        }
        return httpProxyCacheServer as HttpProxyCacheServer
    }
}