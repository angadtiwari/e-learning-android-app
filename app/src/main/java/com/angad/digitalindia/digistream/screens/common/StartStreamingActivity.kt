package com.angad.digitalindia.digistream.screens.common

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.components.AppDialogView
import com.angad.digitalindia.digistream.constants.AppEventsConstants
import com.angad.digitalindia.digistream.extensions.appActivityDialog
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.models.Category
import com.angad.digitalindia.digistream.models.eventbus.Events
import com.angad.digitalindia.digistream.networks.AppNetworkingService
import com.angad.digitalindia.digistream.networks.models.ApiPlaybacks
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipDrawable
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.activity_start_streaming.*
import kotlinx.android.synthetic.main.toolbar_app_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus

class StartStreamingActivity : AppBaseActivity() {
    private var choosenCategories: MutableList<Category> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start_streaming)

        showToolbar = true
        txt_heading.text = "Start Streaming"
        btn_close.visibility = View.VISIBLE
        initListeners()
        initCategoryChips()
    }

    private fun initCategoryChips() {
        preference.prefCategories?.let {
            choosenCategories = it.toMutableList()
            choosenCategories.forEach {category ->
                addCategoryChip(chipGroupCategory, category)
            }
        }
    }

    private fun addCategoryChip(chipGroup: ChipGroup, category: Category) {
        val chip = Chip(this)
        chip.setChipDrawable(ChipDrawable.createFromResource(this, R.xml.chip_category))
        chip.text = category.name
        chip.isChecked = false
        chip.setOnCheckedChangeListener { compoundButton, b -> category.selected = b }
        chipGroup.addView(chip)
    }

    private fun initListeners() {
        btn_continue.setOnClickListener {
            if(edit_playback_title.text.trim().toString().isNullOrBlank()) {
                appActivityDialog(resources.getString(R.string.app_name), "Title required...", AppDialogView.ERROR)
                return@setOnClickListener
            }
            if(choosenCategories.firstOrNull { it.selected }?._id.isNullOrBlank()) {
                appActivityDialog(resources.getString(R.string.app_name), "Category required...", AppDialogView.ERROR)
                return@setOnClickListener
            }
            val apiStreamingRequest = ApiPlaybacks.ApiStreaming.Request(
                title = edit_playback_title.text.trim().toString(),
                category = choosenCategories.firstOrNull { it.selected }?._id!!)

            startLoading()
            GlobalScope.launch(Dispatchers.Main) {
                val response = withContext(Dispatchers.Default) {
                    AppNetworkingService.instance.liveStreaming(apiStreamingRequest)
                }
                if (response.isSuccessful && response.body() != null) {
                    val bundle = Bundle()
                    bundle.putParcelable("playback", response.body()!!.data)
                    EventBus.getDefault().post(Events(AppEventsConstants.EVENT_LIVESTREAMING_CREATED, bundle))
                    finish()
                } else {
                    appActivityDialog(resources.getString(R.string.app_name), "Failed to start live streaming. Please write us at admin@digistream.com", AppDialogView.ERROR)
                }
                stopLoading()
            }
        }
    }
}