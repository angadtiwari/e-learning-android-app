package com.angad.digitalindia.digistream.injections

import androidx.lifecycle.ViewModelProvider
import com.angad.digitalindia.digistream.networks.AppNetworkingService
import com.angad.digitalindia.digistream.repositories.ExploreRepository
import com.angad.digitalindia.digistream.repositories.NotificationRepository
import com.angad.digitalindia.digistream.repositories.UploadRepository
import com.angad.digitalindia.digistream.viewmodel.factory.ExploreViewModelFactory
import com.angad.digitalindia.digistream.viewmodel.factory.NotificationViewModelFactory
import com.angad.digitalindia.digistream.viewmodel.factory.UploadViewModelFactory

/**
 * Class that handles object creation.
 * Like this, objects can be passed as parameters in the constructors and then replaced for
 * testing, where needed.
 */
object Injection {

    /**
     * Creates an instance of [GithubRepository] based on the [GithubService] and a
     * [GithubLocalCache]
     */
    private fun provideNotificationRepository(): NotificationRepository {
        return NotificationRepository(AppNetworkingService.instance)
    }

    private fun provideExploreRepository(): ExploreRepository {
        return ExploreRepository(AppNetworkingService.instance)
    }

    private fun provideUploadRepository(): UploadRepository {
        return UploadRepository(AppNetworkingService.instance)
    }

    /**
     * Provides the [ViewModelProvider.Factory] that is then used to get a reference to
     * [ViewModel] objects.
     */
    fun provideNotificationViewModelFactory(): ViewModelProvider.Factory {
        return NotificationViewModelFactory(
            provideNotificationRepository()
        )
    }

    fun provideExploreViewModelFactory(): ViewModelProvider.Factory {
        return ExploreViewModelFactory(
            provideExploreRepository()
        )
    }

    fun provideUploadViewModelFactory(): ViewModelProvider.Factory {
        return UploadViewModelFactory(
            provideUploadRepository()
        )
    }
}