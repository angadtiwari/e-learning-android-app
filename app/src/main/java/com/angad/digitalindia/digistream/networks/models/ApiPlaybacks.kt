package com.angad.digitalindia.digistream.networks.models

import com.angad.digitalindia.digistream.models.Playback

/**
 * Created by Angad Tiwari on 12-07-2020.
 */
class ApiPlaybacks {

    data class Response(
        val code: Int,
        val `data`: List<Playback>,
        val message: String,
        val next_page: Int,
        val status: String
    )

    class ApiStreaming {
        data class Request(
            val category: String,
            val title: String
        )

        data class Response(
            val code: Int,
            val `data`: Playback,
            val message: String,
            val next_page: Any,
            val status: String
        )
    }
}