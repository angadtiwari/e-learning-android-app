package com.angad.digitalindia.digistream.extensions

import android.content.Context
import com.angad.digitalindia.digistream.preferences.AppPreferences
import com.angad.digitalindia.digistream.preferences.AppPreferencesHelper

/**
 * Created by Angad Tiwari on 07-07-2020.
 */

inline val Context.preference: AppPreferences
    get() = AppPreferences(this)