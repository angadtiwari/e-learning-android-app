package com.angad.digitalindia.digistream.paging

import androidx.paging.PagingSource
import com.angad.digitalindia.digistream.constants.AppGlobalConstants
import com.angad.digitalindia.digistream.models.Notification
import com.angad.digitalindia.digistream.networks.AppNetworkingService
import java.lang.RuntimeException

/**
 * Created by Angad Tiwari on 11-07-2020.
 */
class NotificationPagingSource(private val appNetworkingService: AppNetworkingService): PagingSource<Int, Notification>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Notification> {
        try {
            // Start refresh at page 1 if undefined.
            val nextPageNumber = params.key ?: 1
            val response = appNetworkingService.notifications(nextPageNumber, AppGlobalConstants.PAGING_SIZE)
            if(response.isSuccessful) {
                return LoadResult.Page(data = response.body()!!.data, prevKey = null, nextKey = response.body()!!.next_page)
            } else {
                // Handle errors in this block and return LoadResult.Error if it is an
                // expected error (such as a network failure).
                return LoadResult.Error(throwable = RuntimeException(response.errorBody().toString()))
            }
        } catch (e: Exception) {
            // Handle errors in this block and return LoadResult.Error if it is an
            // expected error (such as a network failure).
            return LoadResult.Error(throwable = e.fillInStackTrace())
        }
    }
}