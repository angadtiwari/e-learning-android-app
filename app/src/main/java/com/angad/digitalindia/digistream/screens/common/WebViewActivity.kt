package com.angad.digitalindia.digistream.screens.common

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.extensions.fullscreen
import kotlinx.android.synthetic.main.toolbar_app_activity.*

class WebViewActivity : AppBaseActivity() {
    private var headerTitle: String? = null
    private var url: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        showToolbar = true

        handleIntent()
        btn_close.visibility = View.VISIBLE
        txt_heading.text = headerTitle
        initWebView()
    }

    private fun handleIntent() {
        headerTitle = intent.getStringExtra("title")
        url = intent.getStringExtra("url")
    }

    private fun initWebView() {
        if(url.isNullOrBlank()) {
            finish()
            return
        }
        startLoading()
        val myWebView = findViewById<View>(R.id.webview) as WebView
        val webSettings = myWebView.settings
        webSettings.javaScriptEnabled = true
        myWebView.loadUrl(url!!)
        myWebView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageCommitVisible(view: WebView, url: String) {
                super.onPageCommitVisible(view, url)
            }

            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                this@WebViewActivity.stopLoading()
            }
        }
    }
}