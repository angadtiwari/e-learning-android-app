package com.angad.digitalindia.digistream.repositories

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.angad.digitalindia.digistream.constants.AppGlobalConstants
import com.angad.digitalindia.digistream.models.Playback
import com.angad.digitalindia.digistream.networks.AppNetworkingService
import com.angad.digitalindia.digistream.paging.ExplorePagingSource
import kotlinx.coroutines.flow.Flow

/**
 * Created by Angad Tiwari on 17-07-2020.
 */
class ExploreRepository(private val appNetworkingService: AppNetworkingService) {

    fun getPlaybacks(isGuest: Int = 0): Flow<PagingData<Playback>> {
        return Pager(
            config = PagingConfig(pageSize = AppGlobalConstants.PAGING_SIZE, enablePlaceholders = true),
            pagingSourceFactory = { ExplorePagingSource(appNetworkingService, isGuest) }
        ).flow
    }
}