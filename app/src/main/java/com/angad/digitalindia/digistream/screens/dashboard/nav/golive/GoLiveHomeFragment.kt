package com.angad.digitalindia.digistream.screens.dashboard.nav.golive

import android.Manifest;
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.hardware.Camera
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.base.AppBaseFragment
import com.angad.digitalindia.digistream.constants.AppEventsConstants
import com.angad.digitalindia.digistream.constants.AppRequestCodeConstants.IntentRequest.Companion.REQUEST_CAMERA
import com.angad.digitalindia.digistream.extensions.blinkLiveStreaming
import com.angad.digitalindia.digistream.extensions.fullscreen
import com.angad.digitalindia.digistream.extensions.liveStreamingRTMPUrl
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.models.Playback
import com.angad.digitalindia.digistream.models.eventbus.Events
import com.angad.digitalindia.digistream.screens.common.StartStreamingActivity
import com.angad.digitalindia.digistream.screens.dashboard.DashboardActivity
import com.github.faucamp.simplertmp.RtmpHandler
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.fragment_golive_home.*
import net.ossrs.yasea.SrsCameraView
import net.ossrs.yasea.SrsEncodeHandler
import net.ossrs.yasea.SrsPublisher
import net.ossrs.yasea.SrsRecordHandler

import java.io.IOException
import java.net.SocketException

class GoLiveHomeFragment: AppBaseFragment(), RtmpHandler.RtmpListener, SrsRecordHandler.SrsRecordListener, SrsEncodeHandler.SrsEncodeListener  {

    private val TAG = GoLiveHomeFragment::class.java.simpleName

    private var sp: SharedPreferences? = null
    private val recPath: String = "${android.os.Environment.getExternalStorageDirectory().path.toString()}/${java.lang.System.currentTimeMillis()}.mp4"

    private var mPublisher: SrsPublisher? = null

    private var mWidth = 640
    private var mHeight = 480
    private var isPermissionGranted = false

    private var playback: Playback? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_golive_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        measureCameraDimensions()
        requestPermission()
    }

    private fun measureCameraDimensions() {
        requireActivity().preference.prefDeviceWidth?.let {
            mWidth = it
        }
        requireActivity().preference.prefDeviceHeight?.let {
            mHeight = it
        }
    }

    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= 23 && ((ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED))) {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_CAMERA)
        } else {
            isPermissionGranted = true
            initView()
            initListeners()
        }
    }

    private fun initListeners() {
        btnPublish.setOnClickListener{
            if(btnPublish.tag.toString() == "no_stream") {
                startActivity(Intent(requireContext(), StartStreamingActivity::class.java))
            } else if (btnPublish.tag.toString() == "publish") {
                mPublisher?.startPublish(playback?.stream_key?.liveStreamingRTMPUrl())
                mPublisher?.startCamera()
                img_livestreaming_blink.visibility = View.VISIBLE
                img_livestreaming_blink.blinkLiveStreaming()
                btnPublish.tag = "stop"
                btnPause.isEnabled = true
                btnPublish.setImageResource(R.drawable.exo_icon_stop)
            } else if (btnPublish.tag.toString() == "stop") {
                mPublisher?.stopPublish()
                mPublisher?.stopRecord()
                btnPublish.tag = "publish"
                btnRecord.tag = "record"
                btnPause.isEnabled = false
                btnPublish.setImageResource(R.drawable.ic_nav_golive)
                requireActivity().onBackPressed()
            }
        }
        btnPause.setOnClickListener{
            if (btnPause.tag.toString() == "Pause") {
                mPublisher?.pausePublish()
                btnPause.tag = "resume"
                btnPause.setImageResource(R.drawable.exo_icon_play)
            } else {
                mPublisher?.resumePublish()
                btnPause.tag = "Pause"
                btnPause.setImageResource(R.drawable.exo_icon_pause)
            }
        }
        btnSwitchCamera.setOnClickListener{
            (mPublisher?.cameraId?.plus(1))?.rem(Camera.getNumberOfCameras())?.let { it1 ->
                mPublisher?.switchCameraFace(
                    it1
                )
            }
        }
        btnRecord.setOnClickListener{
            if (btnRecord.tag.toString() == "record") {
                if (mPublisher?.startRecord(recPath)!!) {
                    btnRecord.tag = "pause"
                    btnRecord.setImageResource(R.drawable.exo_icon_pause)
                }
            } else if (btnRecord.tag.toString() == "pause") {
                mPublisher?.pauseRecord()
                btnRecord.tag = "resume"
                btnRecord.setImageResource(R.drawable.exo_icon_play)
            } else if (btnRecord.tag.toString() == "resume") {
                mPublisher?.resumeRecord()
                btnRecord.tag = "pause"
                btnRecord.setImageResource(R.drawable.ic_start_recording)
            }
        }
    }

    private fun initView() {
        btnPause.isEnabled = false
        mPublisher = SrsPublisher(mCameraView)
        mPublisher?.setEncodeHandler(SrsEncodeHandler(this))
        mPublisher?.setRtmpHandler(RtmpHandler(this))
        mPublisher?.setRecordHandler(SrsRecordHandler(this))
        mPublisher?.setPreviewResolution(mHeight, mWidth)
        mPublisher?.setOutputResolution(mWidth, mHeight)
        mPublisher?.setVideoHDMode()
        mPublisher?.startCamera()
        mCameraView.setCameraCallbacksHandler(object : SrsCameraView.CameraCallbacksHandler() {
            override fun onCameraParameters(params: Camera.Parameters?) {
                super.onCameraParameters(params)
            }
        })
    }

    override fun onMessageEvent(events: Events) {
        super.onMessageEvent(events)

        when (events.eventType) {
            AppEventsConstants.NEW_GOLIVE_SESSION -> {
                playback = null
                mPublisher = null
                btnPublish.tag = "no_stream"
                txt_livestream_title.visibility = View.GONE
                txt_livestream_category.visibility = View.GONE
                txt_livestream_title.text = ""
                txt_livestream_category.text = ""
            }
            AppEventsConstants.EVENT_CAMERA_PERMISSION_GRANTED -> {
                isPermissionGranted = true
                initView()
                initListeners()
            }

            AppEventsConstants.EVENT_LIVESTREAMING_CREATED -> {
                events.bundle?.getParcelable<Playback>("playback")?.let {
                    playback = it
                    btnPublish.tag = "publish"
                    txt_livestream_title.visibility = View.VISIBLE
                    txt_livestream_category.visibility = View.VISIBLE
                    txt_livestream_title.text = playback?.title
                    txt_livestream_category.text = playback?.category?.name
                    btnPublish.performClick()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (mPublisher?.camera == null && isPermissionGranted) {
            //if the camera was busy and available again
            mPublisher?.startCamera()
        }
    }

    override fun onResume() {
        super.onResume()
        btnPublish.isEnabled = true
        mPublisher?.resumeRecord()
    }

    override fun onPause() {
        super.onPause()
        mPublisher?.pauseRecord()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPublisher?.stopPublish()
        mPublisher?.stopRecord()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        mPublisher?.stopEncode()
        mPublisher?.stopRecord()
        btnRecord!!.tag = "record"
        mPublisher?.setScreenOrientation(newConfig.orientation)
        if (btnPublish!!.tag.toString().contentEquals("stop")) {
            mPublisher?.startEncode()
        }
        mPublisher?.startCamera()
    }

    private fun handleException(e: Exception): Unit {
        try {
            Toast.makeText(requireContext(), e.message, Toast.LENGTH_SHORT).show()
            mPublisher?.stopPublish()
            mPublisher?.stopRecord()
            btnPublish!!.tag = "publish"
            btnRecord!!.tag = "record"
        } catch (e1: Exception) {
            //
        }
    }

    // Implementation of SrsRtmpListener.
    override fun onRtmpConnecting(msg: String?): Unit {
        //Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
    }

    override fun onRtmpConnected(msg: String?): Unit {
        //Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
    }

    override fun onRtmpVideoStreaming(): Unit {}

    override fun onRtmpAudioStreaming(): Unit {}

    override fun onRtmpStopped(): Unit {
        //Toast.makeText(requireContext(), "Stopped", Toast.LENGTH_SHORT).show()
    }

    override fun onRtmpDisconnected(): Unit {
        //Toast.makeText(requireContext(), "Disconnected", Toast.LENGTH_SHORT).show()
    }

    override fun onRtmpVideoFpsChanged(fps: Double): Unit {
        Log.i(TAG, String.format("Output Fps: %f", fps))
    }

    override fun onRtmpVideoBitrateChanged(bitrate: Double): Unit {
        val rate = bitrate.toInt()
        if (rate / 1000 > 0) {
            Log.i(TAG, String.format("Video bitrate: %f kbps", bitrate / 1000))
        } else {
            Log.i(TAG, String.format("Video bitrate: %d bps", rate))
        }
    }

    override fun onRtmpAudioBitrateChanged(bitrate: Double): Unit {
        val rate = bitrate.toInt()
        if (rate / 1000 > 0) {
            Log.i(TAG, String.format("Audio bitrate: %f kbps", bitrate / 1000))
        } else {
            Log.i(TAG, String.format("Audio bitrate: %d bps", rate))
        }
    }

    override fun onRtmpSocketException(e: SocketException): Unit {
        handleException(e)
    }

    override fun onRtmpIOException(e: IOException): Unit {
        handleException(e)
    }

    override fun onRtmpIllegalArgumentException(e: IllegalArgumentException): Unit {
        handleException(e)
    }

    override fun onRtmpIllegalStateException(e: IllegalStateException): Unit {
        handleException(e)
    }

    // Implementation of SrsRecordHandler.
    override fun onRecordPause(): Unit {
        //Toast.makeText(requireContext(), "Record paused", Toast.LENGTH_SHORT).show()
    }

    override fun onRecordResume(): Unit {
        //Toast.makeText(requireContext(), "Record resumed", Toast.LENGTH_SHORT).show()
    }

    override fun onRecordStarted(msg: String): Unit {
        //Toast.makeText(requireContext(), "Recording file: $msg", Toast.LENGTH_SHORT).show()
    }

    override fun onRecordFinished(msg: String): Unit {
        //Toast.makeText(requireContext(), "MP4 file saved: $msg", Toast.LENGTH_SHORT).show()
    }

    override fun onRecordIOException(e: IOException): Unit {
        handleException(e)
    }

    override fun onRecordIllegalArgumentException(e: IllegalArgumentException): Unit {
        handleException(e)
    }

    // Implementation of SrsEncodeHandler.
    override fun onNetworkWeak(): Unit {
        Toast.makeText(requireContext(), "Weak Network Detected", Toast.LENGTH_SHORT).show()
    }

    override fun onNetworkResume(): Unit {
        //Toast.makeText(requireContext(), "Network resume", Toast.LENGTH_SHORT).show()
    }

    override fun onEncodeIllegalArgumentException(e: IllegalArgumentException): Unit {
        handleException(e)
    }
}