package com.angad.digitalindia.digistream.models

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * Created by Angad Tiwari on 08-07-2020.
 */
@Entity(tableName = "notification", indices = [Index(value = ["_id"], unique = true)])
data class Notification(
    @PrimaryKey val _id: String,
    val id: String,
    val body: String,
    val media: String,
    val time: Long,
    val title: String,
    val playback: Playback,
    val userIds: MutableList<String> = mutableListOf()
)