package com.angad.digitalindia.digistream.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.angad.digitalindia.digistream.models.Playback
import com.angad.digitalindia.digistream.repositories.ExploreRepository
import com.angad.digitalindia.digistream.repositories.UploadRepository
import kotlinx.coroutines.flow.Flow

/**
 * Created by Angad Tiwari on 17-07-2020.
 */
class UploadViewModel(private val uploadRepository: UploadRepository): ViewModel() {
    private var currentNotificationResult: Flow<PagingData<Playback>>? = null

    fun getUploads(): Flow<PagingData<Playback>> {
        val newResult: Flow<PagingData<Playback>> = uploadRepository.getUploads()
            .cachedIn(viewModelScope)
        currentNotificationResult = newResult
        return newResult
    }
}