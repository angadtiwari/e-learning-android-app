package com.angad.digitalindia.digistream.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.angad.digitalindia.digistream.repositories.NotificationRepository
import com.angad.digitalindia.digistream.repositories.UploadRepository
import com.angad.digitalindia.digistream.viewmodel.NotificationViewModel
import com.angad.digitalindia.digistream.viewmodel.UploadViewModel

/**
 * Created by Angad Tiwari on 17-07-2020.
 */
class UploadViewModelFactory(private val uploadRepository: UploadRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UploadViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return UploadViewModel(
                uploadRepository
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}