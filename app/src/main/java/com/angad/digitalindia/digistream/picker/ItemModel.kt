package com.angad.digitalindia.digistream.picker

import android.annotation.SuppressLint
import android.os.Parcelable
import androidx.annotation.IntDef
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class ItemModel(
        @ItemType val type: Long,
        val itemLabel: String = "",
        val itemIcon: Int = 0,
        val hasBackground: Boolean = true,
        @ShapeType val backgroundType: Long = TYPE_CIRCLE,
        val itemBackgroundColor: Int = 0)
    : Parcelable {

    companion object {
        @IntDef(TYPE_CIRCLE.toInt(), TYPE_SQUARE.toInt(), TYPE_ROUNDED_SQUARE.toInt())
        @Retention(AnnotationRetention.SOURCE)
        annotation class ShapeType

        const val TYPE_CIRCLE = 0L
        const val TYPE_SQUARE = 1L
        const val TYPE_ROUNDED_SQUARE = 2L

        @IntDef(ITEM_CAMERA.toInt(), ITEM_GALLERY.toInt(), ITEM_VIDEO.toInt(), ITEM_VIDEO_GALLERY.toInt(), ITEM_FILES.toInt(), ITEM_ALL.toInt())
        @Retention(AnnotationRetention.SOURCE)
        annotation class ItemType

        const val ITEM_CAMERA = 10L
        const val ITEM_GALLERY = 11L
        const val ITEM_VIDEO = 12L
        const val ITEM_VIDEO_GALLERY = 13L
        const val ITEM_FILES = 14L
        const val ITEM_ALL = 15L

    }
}