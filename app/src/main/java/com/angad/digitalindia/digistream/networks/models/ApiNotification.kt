package com.angad.digitalindia.digistream.networks.models

import com.angad.digitalindia.digistream.models.Notification

/**
 * Created by Angad Tiwari on 11-07-2020.
 */
class ApiNotification {
    data class Response(
        val code: Int,
        val `data`: List<Notification>,
        val message: String,
        val status: String,
        val next_page: Int
    )
}