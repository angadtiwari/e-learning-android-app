package com.angad.digitalindia.digistream.streaming

import android.net.Uri
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Angad Tiwari on 07-07-2020.
 */
@Parcelize
data class AppStreamingUrls(val dashUrl: String? = null, val hlsUrl: String? = null, val ssUrl: String? = null, val progressiveUrl: String? = null, val asset: String? = null, val localUri: Uri? = null): Parcelable