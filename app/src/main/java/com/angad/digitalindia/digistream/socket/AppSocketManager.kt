package com.angad.digitalindia.digistream.socket

import android.util.Log
import com.angad.digitalindia.digistream.constants.AppEnvironmentConstants
import io.socket.client.IO
import io.socket.client.Socket

/**
 * Created by Angad Tiwari on 09-07-2020.
 */
class AppSocketManager {

    companion object {
        private var socket: Socket? = null

        fun connectSocket(): Socket? {
            if(socket == null) {
                socket = IO.socket(AppEnvironmentConstants.WebSocketUrls.SOCKET_URL)
            }
            return socket
        }

        fun listenEvent() {
            socket ?: connectSocket()
            socket?.on(Socket.EVENT_CONNECT) {
                Log.d(AppSocketManager::class.java.simpleName, "socket connected")
            }
            socket?.on(Socket.EVENT_DISCONNECT) {
                Log.d(AppSocketManager::class.java.simpleName, "socket disconnected")
            }
            socket?.on(AppEnvironmentConstants.WebSocketEvents.SOCKET_EVENT_PLAYBACK_READY) {
                Log.d(AppSocketManager::class.java.simpleName, "socket event received ${AppEnvironmentConstants.WebSocketEvents.SOCKET_EVENT_PLAYBACK_READY}")
            }
            socket?.on(AppEnvironmentConstants.WebSocketEvents.SOCKET_EVENT_DELETED_READY) {
                Log.d(AppSocketManager::class.java.simpleName, "socket event received ${AppEnvironmentConstants.WebSocketEvents.SOCKET_EVENT_DELETED_READY}")
            }
            socket?.connect()
        }
    }
}