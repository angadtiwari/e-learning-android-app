package com.angad.digitalindia.digistream.db.dao

import androidx.paging.DataSource
import androidx.paging.PagingSource
import androidx.room.*
import com.angad.digitalindia.digistream.models.Notification

/**
 * Created by Angad Tiwari on 11-07-2020.
 */
@Entity
@Dao
interface NotificationDao {
    @Query("SELECT * FROM notification ORDER BY time DESC")
    fun getAll(): PagingSource<Int, Notification>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(notification: Notification)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(notifications: List<Notification>)
}