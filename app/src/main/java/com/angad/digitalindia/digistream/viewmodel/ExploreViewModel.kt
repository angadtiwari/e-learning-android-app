package com.angad.digitalindia.digistream.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.angad.digitalindia.digistream.db.dao.NotificationDao
import com.angad.digitalindia.digistream.models.Notification
import com.angad.digitalindia.digistream.models.Playback
import com.angad.digitalindia.digistream.repositories.ExploreRepository
import com.angad.digitalindia.digistream.repositories.NotificationRepository
import kotlinx.coroutines.flow.Flow

/**
 * Created by Angad Tiwari on 17-07-2020.
 */
class ExploreViewModel(val exploreRepository: ExploreRepository): ViewModel() {
    private var currentNotificationResult: Flow<PagingData<Playback>>? = null

    fun getPlaybacks(isGuest: Int = 0): Flow<PagingData<Playback>> {
        val newResult: Flow<PagingData<Playback>> = exploreRepository.getPlaybacks(isGuest)
            .cachedIn(viewModelScope)
        currentNotificationResult = newResult
        return newResult
    }
}