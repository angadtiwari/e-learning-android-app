package com.angad.digitalindia.digistream.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.angad.digitalindia.digistream.repositories.NotificationRepository
import com.angad.digitalindia.digistream.viewmodel.NotificationViewModel

/**
 * Created by Angad Tiwari on 11-07-2020.
 */

/**
 * Factory for ViewModels
 */
class NotificationViewModelFactory(private val notificationRepository: NotificationRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NotificationViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return NotificationViewModel(
                notificationRepository
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}