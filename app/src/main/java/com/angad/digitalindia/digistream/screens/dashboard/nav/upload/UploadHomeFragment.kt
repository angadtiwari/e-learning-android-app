package com.angad.digitalindia.digistream.screens.dashboard.nav.upload

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.base.AppBaseFragment
import com.angad.digitalindia.digistream.extensions.normalscreen
import com.angad.digitalindia.digistream.injections.Injection
import com.angad.digitalindia.digistream.models.PlaybackId
import com.angad.digitalindia.digistream.models.Playback
import com.angad.digitalindia.digistream.models.eventbus.Events
import com.angad.digitalindia.digistream.networks.AppNetworkingService
import com.angad.digitalindia.digistream.picker.ItemModel
import com.angad.digitalindia.digistream.picker.PickerDialog
import com.angad.digitalindia.digistream.screens.common.UploadPlaybackActivity
import com.angad.digitalindia.digistream.screens.dashboard.DashboardActivity
import com.angad.digitalindia.digistream.screens.dashboard.nav.explore.ExploreItemAdapter
import com.angad.digitalindia.digistream.viewmodel.ExploreViewModel
import com.angad.digitalindia.digistream.viewmodel.UploadViewModel
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.fragment_notification_home.*
import kotlinx.android.synthetic.main.fragment_upload_home.*
import kotlinx.android.synthetic.main.toolbar_app_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.ArrayList

class UploadHomeFragment: AppBaseFragment() {

    private lateinit var adapterUpload: UploadItemAdapter
    private lateinit var viewModel: UploadViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_upload_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as DashboardActivity).txt_heading.text = resources.getString(R.string.title_upload)

        initUploadRecyler()
        fetchUploadList()
        initListeners()
    }

    private fun fetchUploadList() {
        progress = if(swiperefresh_upload.isRefreshing.not()) View.VISIBLE else View.GONE
        lifecycleScope.launch {
            viewModel.getUploads().collectLatest { pagingData ->
                GlobalScope.launch(Dispatchers.IO) {
                    //TODO: need to insert notification paging data into room db
                }
                adapterUpload.submitData(pagingData)
            }
        }
    }

    private fun initUploadRecyler() {
        viewModel = ViewModelProvider(this, Injection.provideUploadViewModelFactory()).get(UploadViewModel::class.java)
        adapterUpload = UploadItemAdapter(requireActivity(), object: DiffUtil.ItemCallback<Playback>() {
            override fun areItemsTheSame(oldItem: Playback, newItem: Playback): Boolean {
                return oldItem._id == newItem._id
            }

            override fun areContentsTheSame(oldItem: Playback, newItem: Playback): Boolean {
                return oldItem._id == newItem._id
            }
        })
        recycler_upload.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recycler_upload.adapter = adapterUpload

        GlobalScope.launch(Dispatchers.Main) {
            adapterUpload.loadStateFlow.collectLatest { loadStates ->
                progress = if (loadStates.refresh is LoadState.Loading && swiperefresh_upload.isRefreshing.not()) View.VISIBLE else View.GONE
                layout_upload_emptyview?.visibility = if(loadStates.refresh !is LoadState.Loading && adapterUpload.itemCount == 0) View.VISIBLE else View.GONE
                swiperefresh_upload?.isRefreshing = loadStates.refresh is LoadState.Loading && progress != View.VISIBLE
            }
        }
    }

    private fun initListeners() {
        fab_upload_playback.setOnClickListener {
            requireActivity().startActivity(Intent(requireActivity(), UploadPlaybackActivity::class.java))
        }

        swiperefresh_upload.setOnRefreshListener {
            fetchUploadList()
        }
    }

    override fun onMessageEvent(events: Events) {
        super.onMessageEvent(events)

        when (events.eventType) {

        }
    }
}