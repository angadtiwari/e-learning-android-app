package com.angad.digitalindia.digistream.extensions

import android.app.AlertDialog
import android.content.Context
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.components.AppDialogView
import kotlinx.android.synthetic.main.dialog_app.view.*

/**
 * Created by Angad Tiwari on 08-07-2020.
 */

fun Context.appContextDialog(title: String, msg: String, type: Int, checkLastNoNetworkMsg: Boolean = true, showPositiveBtn: Boolean = false, showNegativeBtn: Boolean = true,
                             positiveText: String? = null, negativeText: String? = null,
                             negativeFunc: (() -> Unit)? = null, positiveFunc: (() -> Unit)? = null) {
    /*if(checkLastNoNetworkMsg && msg == resources.getString(R.string.common_text_network_error) && noNetworkAlertAlreadyShown()) {
        return
    } else {
        this.preference.last_nonetwork_time = System.currentTimeMillis()
    }*/
    showMessageAlertDialog(this) {
        header.text = title
        when (type) {
            AppDialogView.SUCCESS -> header.setTextColor(resources.getColor(R.color.colorPrimaryDark))
            AppDialogView.ERROR -> header.setTextColor(resources.getColor(R.color.colorError))
            else -> header.setTextColor(resources.getColor(R.color.colorAccent))
        }

        dialogView.btn_positive.visibility = if(showPositiveBtn) View.VISIBLE else View.GONE
        dialogView.btn_negative.visibility = if(showNegativeBtn) View.VISIBLE else View.GONE
        positiveText?.let {
            dialogView.btn_positive.setText(it)
        }
        negativeText?.let {
            dialogView.btn_negative.setText(it)
        }
        message.text = msg
        cancelable = false
        closeIconClickListener {  }
        onNegativeButtonClickListener {
            negativeFunc?.invoke()
        }
        onPositiveButtonClickListener {
            positiveFunc?.invoke()
        }
    }.show()
}

fun Fragment.appFragmentDialog(title: String, msg: String, type: Int, checkLastNoNetworkMsg: Boolean = true, showPositiveBtn: Boolean = false, showNegativeBtn: Boolean = true,
                               positiveText: String? = null, negativeText: String? = null,
                               negativeFunc: (() -> Unit)? = null, positiveFunc: (() -> Unit)? = null) {
    if(context == null) {
        return
    }
    context!!.appContextDialog(title, msg, type, checkLastNoNetworkMsg, showPositiveBtn, showNegativeBtn, positiveText, negativeText, negativeFunc, positiveFunc)
}

fun AppCompatActivity.appActivityDialog(title: String, msg: String, type: Int, checkLastNoNetworkMsg: Boolean = true, showPositiveBtn: Boolean = false, showNegativeBtn: Boolean = true,
                                        positiveText: String? = null, negativeText: String? = null,
                                        negativeFunc: (() -> Unit)? = null, positiveFunc: (() -> Unit)? = null) {
    appContextDialog(title, msg, type, checkLastNoNetworkMsg, showPositiveBtn, showNegativeBtn, positiveText, negativeText, negativeFunc, positiveFunc)
}

fun AppBaseActivity.appActivityDialog(title: String, msg: String, type: Int, checkLastNoNetworkMsg: Boolean = true, showPositiveBtn: Boolean = false, showNegativeBtn: Boolean = true,
                                           positiveText: String? = null, negativeText: String? = null,
                                           negativeFunc: (() -> Unit)? = null, positiveFunc: (() -> Unit)? = null) {
    appContextDialog(title, msg, type, checkLastNoNetworkMsg, showPositiveBtn, showNegativeBtn, positiveText, negativeText, negativeFunc, positiveFunc)
}

fun showMessageAlertDialog(context: Context, func: AppDialogView.() -> Unit): AlertDialog =
    AppDialogView(context).apply {
        func()
    }.create()