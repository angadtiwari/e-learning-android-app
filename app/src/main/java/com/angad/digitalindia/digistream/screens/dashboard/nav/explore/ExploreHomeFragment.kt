package com.angad.digitalindia.digistream.screens.dashboard.nav.explore

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseFragment
import com.angad.digitalindia.digistream.constants.AppSocialAccountConstants
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.injections.Injection
import com.angad.digitalindia.digistream.models.Playback
import com.angad.digitalindia.digistream.models.eventbus.Events
import com.angad.digitalindia.digistream.screens.dashboard.DashboardActivity
import com.angad.digitalindia.digistream.viewmodel.ExploreViewModel
import kotlinx.android.synthetic.main.fragment_explore_home.*
import kotlinx.android.synthetic.main.toolbar_app_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class ExploreHomeFragment: AppBaseFragment() {

    private lateinit var viewModel: ExploreViewModel
    private lateinit var adapterExplore: ExploreItemAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_explore_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as DashboardActivity).txt_heading.text = resources.getString(R.string.title_explore)

        initExploreRecyler()
        fetchExploreList()
        initListeners()
    }

    private fun fetchExploreList() {
        progress = if(swiperefresh_explore.isRefreshing.not()) View.VISIBLE else View.GONE
        lifecycleScope.launch {
            val isGuest = if(requireActivity().preference.prefAppUser?.provider == AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) 1 else 0
            viewModel.getPlaybacks(isGuest).collectLatest { pagingData ->
                GlobalScope.launch(Dispatchers.IO) {
                    //TODO: need to insert notification paging data into room db
                }
                adapterExplore.submitData(pagingData)
            }
        }
    }

    private fun initExploreRecyler() {
        viewModel = ViewModelProvider(this, Injection.provideExploreViewModelFactory()).get(ExploreViewModel::class.java)
        adapterExplore = ExploreItemAdapter(requireActivity(), object: DiffUtil.ItemCallback<Playback>() {
            override fun areItemsTheSame(oldItem: Playback, newItem: Playback): Boolean {
                return oldItem._id == newItem._id
            }

            override fun areContentsTheSame(oldItem: Playback, newItem: Playback): Boolean {
                return oldItem._id == newItem._id
            }
        })
        recycler_explore.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recycler_explore.adapter = adapterExplore

        GlobalScope.launch(Dispatchers.Main) {
            adapterExplore.loadStateFlow.collectLatest { loadStates ->
                progress = if (loadStates.refresh is LoadState.Loading && swiperefresh_explore.isRefreshing.not()) View.VISIBLE else View.GONE
                layout_explore_emptyview?.visibility = if(loadStates.refresh !is LoadState.Loading && adapterExplore.itemCount == 0) View.VISIBLE else View.GONE
                swiperefresh_explore?.isRefreshing = loadStates.refresh is LoadState.Loading && progress != View.VISIBLE
            }
        }
    }

    private fun initListeners() {
        swiperefresh_explore.setOnRefreshListener {
            fetchExploreList()
        }
    }

    override fun onMessageEvent(events: Events) {
        super.onMessageEvent(events)

        when (events.eventType) {

        }
    }
}