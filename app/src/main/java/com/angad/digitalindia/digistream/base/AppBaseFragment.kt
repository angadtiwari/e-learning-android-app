package com.angad.digitalindia.digistream.base

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.models.eventbus.Events
import com.angad.digitalindia.digistream.networks.AppNetworkingService
import kotlinx.android.synthetic.main.activity_app_base.*
import kotlinx.android.synthetic.main.activity_app_base.progressbar
import kotlinx.android.synthetic.main.fragment_app_base.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Created by Angad Tiwari on 09-07-2020.
 */
open class AppBaseFragment: Fragment(), LifecycleOwner {

    var activity: AppBaseActivity? = null

    inline var apiImplementation: AppBaseActivity.IApiImplementation?
        get() {
            return activity?.let {
                it.apiImplementation
            }
        }
        set(value) {

        }

    override fun onStart() {
        super.onStart()
        if (EventBus.getDefault().isRegistered(this).not()) {
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AppBaseActivity) {
            activity = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        activity = null
    }

    open fun startLoading() {
        activity?.let {
            it.progress_bar.visibility = View.VISIBLE
        }
    }

    open fun stopLoading() {
        activity?.let {
            it.progress_bar.visibility = View.GONE
        }
    }

    inline var progress: Int?
        get() {
            return progressbar_base_fragment?.visibility
        }
        set(show) {
            show?.let {
                progressbar_base_fragment?.visibility = it
            }
        }


    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onMessageEvent(events: Events) {
        Log.d("onMessageEvent", "${events.eventType}")
    }

    open fun clearLoginSession() {
        activity?.let {
            if(it is AppBaseActivity) {
                it.clearLoginSession()
            }
        }
    }
}