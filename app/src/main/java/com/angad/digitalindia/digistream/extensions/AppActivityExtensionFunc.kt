package com.angad.digitalindia.digistream.extensions

import android.os.Build
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by Angad Tiwari on 05-07-2020.
 */
fun AppCompatActivity.fullscreen() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
    }
}

fun AppCompatActivity.normalscreen() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}