package com.angad.digitalindia.digistream.repositories

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.angad.digitalindia.digistream.constants.AppGlobalConstants
import com.angad.digitalindia.digistream.models.Notification
import com.angad.digitalindia.digistream.networks.AppNetworkingService
import com.angad.digitalindia.digistream.paging.NotificationPagingSource
import kotlinx.coroutines.flow.Flow

/**
 * Created by Angad Tiwari on 11-07-2020.
 */
class NotificationRepository(private val appNetworkingService: AppNetworkingService) {

    fun getNotifications(): Flow<PagingData<Notification>> {
        return Pager(
            config = PagingConfig(pageSize = AppGlobalConstants.PAGING_SIZE, enablePlaceholders = true),
            pagingSourceFactory = {NotificationPagingSource(appNetworkingService)}
        ).flow
    }
}