package com.angad.digitalindia.digistream.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Angad Tiwari on 11-07-2020.
 */
@Parcelize
data class ProfileType(
    val _id: String,
    val type: String,
    val description: String,
    var required_name: Boolean = false,
    var selected: Boolean = false
): Parcelable