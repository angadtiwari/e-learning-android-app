package com.angad.digitalindia.digistream.components

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.angad.digitalindia.digistream.R
import mehdi.sakout.fancybuttons.FancyButton

/**
 * Created by Angad Tiwari on 08-07-2020.
 */
class AppDialogView(context: Context) : BaseDialogHelper() {

    companion object {
        val SUCCESS = 1
        val INFO = 0
        val ERROR = -1

        var instance: AppDialogView? = null

        fun singleInstance(context: Context): AppDialogView {
            if(instance == null) {
                instance = AppDialogView(context)
            }
            return instance as AppDialogView
        }
    }

    //  dialog view
    override val dialogView: View by lazy {
        LayoutInflater.from(context).inflate(R.layout.dialog_app, null)
    }

    override val builder: AlertDialog.Builder = AlertDialog.Builder(context).setView(dialogView)

    //  title text
    public val header: TextView by lazy {
        dialogView.findViewById<TextView>(R.id.txt_title)
    }

    //  message text
    public val message: TextView by lazy {
        dialogView.findViewById<TextView>(R.id.text_message)
    }

    //  close icon
    private val closeIcon: ImageView by lazy {
        dialogView.findViewById<ImageView>(R.id.close_icon)
    }

    public val btn_positive: FancyButton by lazy {
        dialogView.findViewById<FancyButton>(R.id.btn_positive)
    }

    public val btn_negative: FancyButton by lazy {
        dialogView.findViewById<FancyButton>(R.id.btn_negative)
    }

    //  closeIconClickListener with listener
    fun closeIconClickListener(func: (() -> Unit)? = null) =
        with(closeIcon) {
            setClickListenerToDialogIcon(func)
        }

    //  closeIconClickListener with listener
    fun onNegativeButtonClickListener(func: (() -> Unit)? = null) =
        with(btn_negative) {
            setClickListenerToDialogIcon(func)
        }

    //  closeIconClickListener with listener
    fun onPositiveButtonClickListener(func: (() -> Unit)? = null) =
        with(btn_positive) {
            setClickListenerToDialogIcon(func)
        }

    //  view click listener as extension function
    private fun View.setClickListenerToDialogIcon(func: (() -> Unit)?) =
        setOnClickListener {
            func?.invoke()
            dialog?.dismiss()
        }
}

abstract class BaseDialogHelper {

    abstract val dialogView: View
    abstract val builder: AlertDialog.Builder

    //  required bools
    open var cancelable: Boolean = true
    open var isBackGroundTransparent: Boolean = true

    //  dialog
    open var dialog: AlertDialog? = null

    //  dialog create
    open fun create(): AlertDialog {
        if(dialog == null) {
            dialog = builder
                .setCancelable(cancelable)
                .create()

            //  very much needed for customised dialogs
            if (isBackGroundTransparent)
                dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        return dialog!!
    }

    //  cancel listener
    open fun onCancelListener(func: () -> Unit): AlertDialog.Builder? =
        builder.setOnCancelListener {
            func()
        }
}