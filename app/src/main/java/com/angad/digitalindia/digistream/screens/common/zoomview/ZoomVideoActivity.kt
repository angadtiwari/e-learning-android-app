package com.angad.digitalindia.digistream.screens.common.zoomview

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.extensions.blinkLiveStreaming
import com.angad.digitalindia.digistream.extensions.fullscreen
import com.angad.digitalindia.digistream.extensions.playbackUrl
import com.angad.digitalindia.digistream.streaming.AppStreamManager
import com.angad.digitalindia.digistream.streaming.AppStreamingUrls
import com.google.android.exoplayer2.SimpleExoPlayer
import kotlinx.android.synthetic.main.activity_zoom_video.*
import kotlinx.android.synthetic.main.adapter_explore_item.view.*

/**
 * Created by Angad Tiwari on 10-07-2020.
 */
class ZoomVideoActivity: AppBaseActivity() {

    private lateinit var simpleExoPlayer: SimpleExoPlayer
    private lateinit var streamingManager: AppStreamManager

    private var playbackId: String? = null
    private var playbackUrl: String? = null
    private var livestream: Boolean = false
    private var streamingUrls: AppStreamingUrls? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fullscreen()
        setContentView(R.layout.activity_zoom_video)
        showToolbar = false

        handleIntent()
        bindData()
    }

    private fun handleIntent() {
        playbackId = intent.getStringExtra("playbackId")
        playbackUrl = intent.getStringExtra("playbackUrl")
        livestream = intent.getBooleanExtra("livestream", false)

        if(playbackId.isNullOrBlank().not()) {
            streamingUrls = AppStreamingUrls(hlsUrl = playbackId?.playbackUrl())
        } else if(playbackUrl.isNullOrBlank().not()) {
            streamingUrls = AppStreamingUrls(localUri = Uri.parse(playbackUrl!!))
        }
    }

    fun bindData() {
        streamingUrls?.let {
            streamingManager = AppStreamManager(this)
            simpleExoPlayer = streamingManager.initZoomPlayer(zoom_video, it)
        }
        if(livestream) {
            img_livestreaming_blink.visibility = View.VISIBLE
            img_livestreaming_blink.blinkLiveStreaming()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        streamingManager.releasePlayer()
    }
}