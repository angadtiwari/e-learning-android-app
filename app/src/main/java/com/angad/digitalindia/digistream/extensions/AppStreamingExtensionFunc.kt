package com.angad.digitalindia.digistream.extensions

import android.content.Context
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.widget.ImageView

/**
 * Created by Angad Tiwari on 12-07-2020.
 */
fun String.playbackThumbnailUrl(): String {
    return "https://image.mux.com/${this}/thumbnail.png?fit_mode=pad"
}
fun String.liveStreamingDefaultThumbnailUrl(): String {
    return "https://videoplayer.telvue.com/assets/placeholder_stream_for_white_background-b77d2d3a176c18f3b4c4bc3176ced66eece0edf85748ff8d15cf3fd108c84301.png"
}

fun String.playbackUrl(): String {
    return "https://stream.mux.com/${this}.m3u8"
}

fun String.liveStreamingRTMPUrl(): String {
    return "rtmp://global-live.mux.com:5222/app/${this}"
}

fun ImageView.blinkLiveStreaming() {
    val animation: Animation = AlphaAnimation(1.0f, 0.0f) //to change visibility from visible to invisible
    animation.duration = 600 //1 second duration for each animation cycle
    animation.interpolator = LinearInterpolator()
    animation.repeatCount = Animation.INFINITE //repeating indefinitely
    animation.repeatMode = Animation.REVERSE //animation will start from end point once ended.
    this.startAnimation(animation) //to start animation

}