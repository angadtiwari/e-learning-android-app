package com.angad.digitalindia.digistream.screens.onboarding.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.auth.socialaccount.FacebookAuth
import com.angad.digitalindia.digistream.auth.socialaccount.GoogleAuth
import com.angad.digitalindia.digistream.auth.socialaccount.GuestAuth
import com.angad.digitalindia.digistream.base.AppBaseFragment
import com.angad.digitalindia.digistream.models.eventbus.Events
import kotlinx.android.synthetic.main.activity_onboarding.*
import kotlinx.android.synthetic.main.fragment_signin.*

/**
 * Created by Angad Tiwari on 09-07-2020.
 */
class SigninFragment: AppBaseFragment(), View.OnClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_signin, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initListeners()
    }

    private fun initListeners() {
        btn_google.setOnClickListener(this)
        btn_facebook.setOnClickListener(this)
        btn_join_as_guest.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when(view.id) {
            R.id.btn_google -> {
                GoogleAuth.instance.loginRequest(requireActivity())
            }
            R.id.btn_facebook -> {
                FacebookAuth.instance.login(requireActivity())
            }
            R.id.btn_join_as_guest -> {
                GuestAuth.instance.login(requireActivity())
            }
        }
    }

    override fun onMessageEvent(events: Events) {
        super.onMessageEvent(events)

        when (events.eventType) {

        }
    }
}