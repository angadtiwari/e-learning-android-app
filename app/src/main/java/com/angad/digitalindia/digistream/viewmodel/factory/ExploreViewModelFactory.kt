package com.angad.digitalindia.digistream.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.angad.digitalindia.digistream.repositories.ExploreRepository
import com.angad.digitalindia.digistream.repositories.NotificationRepository
import com.angad.digitalindia.digistream.viewmodel.ExploreViewModel
import com.angad.digitalindia.digistream.viewmodel.NotificationViewModel

/**
 * Created by Angad Tiwari on 17-07-2020.
 */
class ExploreViewModelFactory(private val exploreRepository: ExploreRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ExploreViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ExploreViewModel(
                exploreRepository
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}