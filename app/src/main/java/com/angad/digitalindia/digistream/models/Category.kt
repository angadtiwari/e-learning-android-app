package com.angad.digitalindia.digistream.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Angad Tiwari on 07-07-2020.
 */
@Parcelize
data class Category(
    val _id: String,
    val name: String,
    val description: String,
    var selected: Boolean = false
): Parcelable