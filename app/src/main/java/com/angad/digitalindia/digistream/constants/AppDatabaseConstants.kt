package com.angad.digitalindia.digistream.constants

/**
 * Created by Angad Tiwari on 11-07-2020.
 */
class AppDatabaseConstants {

    companion object {
        const val DB_NAME = "digi_stream_db"
    }
}