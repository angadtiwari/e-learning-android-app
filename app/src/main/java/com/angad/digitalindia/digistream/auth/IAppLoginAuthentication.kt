package com.angad.digitalindia.digistream.auth

import android.content.Intent
import androidx.fragment.app.FragmentActivity

/**
 * Created by Angad Tiwari on 07-07-2020.
 */
interface IAppLoginAuthentication {
    fun loginRequest(activity: FragmentActivity)
    fun login(activity: FragmentActivity, iOnAuthCallback: IOnAuthCallback? = activity as IOnAuthCallback, intent: Intent? = null)
    fun logout()
}