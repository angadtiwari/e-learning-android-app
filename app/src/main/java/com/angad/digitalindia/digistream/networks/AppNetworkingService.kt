package com.angad.digitalindia.digistream.networks

import com.angad.digitalindia.digistream.BuildConfig
import com.angad.digitalindia.digistream.base.AppDelegate
import com.angad.digitalindia.digistream.constants.AppEnvironmentConstants
import com.angad.digitalindia.digistream.constants.AppGlobalConstants
import com.angad.digitalindia.digistream.networks.interceptors.HeaderSupportInterceptor
import com.angad.digitalindia.digistream.networks.models.ApiNotification
import com.angad.digitalindia.digistream.networks.models.ApiPlaybacks
import com.angad.digitalindia.digistream.networks.models.ApiProfile
import com.angad.digitalindia.digistream.networks.models.ApiUpload
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


/**
 * Created by Angad Tiwari on 07-07-2020.
 */
interface AppNetworkingService {

    companion object {
        val instance: AppNetworkingService by lazy {
            retrofit.create(
                AppNetworkingService::class.java)
        }

        val retrofit: Retrofit by lazy {
            val context = AppDelegate.instance().applicationContext

            val okHttpClientBuilder = OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.MINUTES)
                .writeTimeout(20, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.MINUTES)
            okHttpClientBuilder.addInterceptor(HeaderSupportInterceptor(context))
                .followRedirects(false)
                .followSslRedirects(false)
            if(BuildConfig.DEBUG) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                okHttpClientBuilder.addInterceptor(interceptor)
            }
            val okHttpClient = okHttpClientBuilder.build()

            Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(AppEnvironmentConstants.WebApiUrls.API_URL_V1)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }

    @GET(AppEnvironmentConstants.WebApiEndpoints.ENDPOINT_CATEGORIES)
    suspend fun categories(): Response<ApiProfile.ApiCategory.Response>

    @GET(AppEnvironmentConstants.WebApiEndpoints.ENDPOINT_PROFILETYPES)
    suspend fun profileTypes(): Response<ApiProfile.ApiType.Response>

    @POST(AppEnvironmentConstants.WebApiEndpoints.ENDPOINT_USER_LOGIN)
    suspend fun login(@Body request: ApiProfile.ApiLogin.Request): Response<ApiProfile.Response>

    @PUT(AppEnvironmentConstants.WebApiEndpoints.ENDPOINT_USER_PROFILE_UPDATE)
    suspend fun updateProfile(@Body request: ApiProfile.Request): Response<ApiProfile.Response>

    @GET(AppEnvironmentConstants.WebApiEndpoints.ENDPOINT_USER_PROFILE_UPDATE)
    suspend fun userProfile(): Response<ApiProfile.Response>

    @GET(AppEnvironmentConstants.WebApiEndpoints.ENDPOINT_USER_NOTIFICATION)
    suspend fun notifications(@Query("page") page: Int, @Query("size") size: Int = AppGlobalConstants.PAGING_SIZE): Response<ApiNotification.Response>

    @GET(AppEnvironmentConstants.WebApiEndpoints.ENDPOINT_USER_PLAYBACKS)
    suspend fun playbacks(@Query("page") page: Int = 1, @Query("size") size: Int = AppGlobalConstants.PAGING_SIZE, @Query("isGuest") isGuest: Int = 0): Response<ApiPlaybacks.Response>

    @GET(AppEnvironmentConstants.WebApiEndpoints.ENDPOINT_USER_UPLOADS)
    suspend fun userUploads(@Query("page") page: Int = 1, @Query("size") size: Int = AppGlobalConstants.PAGING_SIZE): Response<ApiPlaybacks.Response>

    @Multipart
    @POST(AppEnvironmentConstants.WebApiEndpoints.ENDPOINT_USER_PLAYBACK_UPLOAD)
    suspend fun upload(@PartMap partMap: MutableMap<String, RequestBody>, @Part playback: MultipartBody.Part): Response<ApiUpload.Response>

    @POST(AppEnvironmentConstants.WebApiEndpoints.ENDPOINT_USER_LIVESTREAMING)
    suspend fun liveStreaming(@Body request: ApiPlaybacks.ApiStreaming.Request): Response<ApiPlaybacks.ApiStreaming.Response>
}