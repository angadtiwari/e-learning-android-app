package com.angad.digitalindia.digistream.screens.onboarding

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import com.angad.digitalindia.digistream.R
import com.angad.digitalindia.digistream.auth.IOnAuthCallback
import com.angad.digitalindia.digistream.auth.socialaccount.FacebookAuth
import com.angad.digitalindia.digistream.auth.socialaccount.GoogleAuth
import com.angad.digitalindia.digistream.base.AppBaseActivity
import com.angad.digitalindia.digistream.constants.AppEventsConstants
import com.angad.digitalindia.digistream.constants.AppGlobalConstants
import com.angad.digitalindia.digistream.constants.AppRequestCodeConstants
import com.angad.digitalindia.digistream.constants.AppSocialAccountConstants
import com.angad.digitalindia.digistream.extensions.fullscreen
import com.angad.digitalindia.digistream.extensions.preference
import com.angad.digitalindia.digistream.models.User
import com.angad.digitalindia.digistream.models.eventbus.Events
import com.angad.digitalindia.digistream.networks.AppNetworkingService
import com.angad.digitalindia.digistream.networks.models.ApiProfile
import com.angad.digitalindia.digistream.screens.dashboard.DashboardActivity
import com.angad.digitalindia.digistream.streaming.AppStreamManager
import com.angad.digitalindia.digistream.streaming.AppStreamingUrls
import kotlinx.android.synthetic.main.activity_onboarding.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class OnboardingActivity: AppBaseActivity(), IOnAuthCallback {
    private lateinit var fragmentSignin: Fragment
    private lateinit var fragmentCategory: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fullscreen()
        setContentView(R.layout.activity_onboarding)
        showToolbar = false

        fragmentCategory = supportFragmentManager.findFragmentById(R.id.fragment_category)!!
        fragmentSignin = supportFragmentManager.findFragmentById(R.id.fragment_signin)!!

        supportFragmentManager.beginTransaction()
            .hide(fragmentCategory)
            .show(fragmentSignin)
            .commit()

        initOnboardingVideoStreaming()

        GlobalScope.launch(Dispatchers.IO) {
            apiImplementation.fetchCategories()
            apiImplementation.fetchProfileTypes()
        }
    }

    private fun initOnboardingVideoStreaming() {
        val streamingUrls = AppStreamingUrls(asset = AppGlobalConstants.ONBOARDING_BGVIDEO_NAME)
        AppStreamManager(this).initOnboardingPlayer(playerView = videoView, onboardingStreamingUrls = streamingUrls)
    }

    override fun onSuccess(userProfile: User.Profile) {
        if(preference.prefAppUser?.provider.isNullOrBlank() || preference.prefAppUser?.provider == AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
            processLogin(userProfile)
            return
        }
        startLoading()
        GlobalScope.launch(Dispatchers.Main) {
            val loginRequest = createLoginRequest(userProfile)
            val response = withContext(Dispatchers.Default) {
                AppNetworkingService.instance.login(loginRequest)
            }
            if(response.isSuccessful) {
                response.body()?.let {
                    preference.prefAppUser = it.data
                    processLogin(it.data)
                }
            } else {
                stopLoading()
            }
        }
    }

    private fun createLoginRequest(userProfile: User.Profile): ApiProfile.ApiLogin.Request {
        return ApiProfile.ApiLogin.Request(
            id = userProfile.id,
            first_name = userProfile.first_name,
            last_name = userProfile.last_name,
            email = userProfile.email,
            birthday = userProfile.birthday,
            gender = userProfile.gender,
            link = userProfile.link,
            locale = userProfile.locale,
            location = userProfile.location,
            name = userProfile.name,
            timezone = userProfile.timezone,
            updated_time = userProfile.updated_time,
            verified = userProfile.verified,
            provider = userProfile.provider,
            profile_pic = userProfile.profile_pic,
            social_account_token = userProfile.social_account_token,
            access_token = userProfile.access_token,
            push_token = userProfile.push_token
        )
    }

    private fun processLogin(data: User.Profile) {
        if(data.categories.isNullOrEmpty() || data.type == null) {
            supportFragmentManager.beginTransaction()
                .hide(fragmentSignin)
                .show(fragmentCategory)
                .commit()
            stopLoading()
        } else {
            val intent = Intent(this@OnboardingActivity, DashboardActivity::class.java)
            startActivity(intent)
            finish()
            preference.prefAppUser?.provider?.let {
                if(it != AppSocialAccountConstants.ProviderName.PROVIDER_GUEST) {
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK  or Intent.FLAG_ACTIVITY_CLEAR_TASK
                }
            }
            overridePendingTransition(0, android.R.anim.fade_out)
        }
    }

    override fun onFailure(msg: String) {
        preference.prefAppUser = null
        Log.d(OnboardingActivity::class.java.simpleName, "failure")
        supportFragmentManager.beginTransaction()
            .show(fragmentSignin)
            .hide(fragmentCategory)
            .commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            AppRequestCodeConstants.IntentRequest.REQUEST_GOOGLE_SIGNIN -> {
                GoogleAuth.instance.login(this, this, data)
            }
            else -> {
                // Pass the activity result back to the Facebook SDK
                FacebookAuth.instance.callbackManager?.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    override fun onMessageEvent(events: Events?) {
        super.onMessageEvent(events)

        when (events?.eventType) {
            AppEventsConstants.EVENT_REGISTRATION -> {
                supportFragmentManager.beginTransaction()
                    .hide(fragmentCategory)
                    .hide(fragmentSignin)
                    .commit()
            }
        }
    }
}