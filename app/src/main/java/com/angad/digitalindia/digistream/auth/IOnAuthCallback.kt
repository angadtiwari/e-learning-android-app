package com.angad.digitalindia.digistream.auth

import com.angad.digitalindia.digistream.models.User

/**
 * Created by Angad Tiwari on 07-07-2020.
 */
interface IOnAuthCallback {
    fun onSuccess(userProfile: User.Profile)
    fun onFailure(msg: String)
}