package com.angad.digitalindia.digistream.networks.models

import com.angad.digitalindia.digistream.models.Playback

/**
 * Created by Angad Tiwari on 13-07-2020.
 */
class ApiUpload {

    data class Response(
        val code: Int,
        val `data`: Playback,
        val message: String,
        val status: String
    )
}