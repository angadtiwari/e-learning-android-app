package com.angad.digitalindia.digistream.models.eventbus

import android.os.Bundle

/**
 * Created by Angad Tiwari on 05-07-2020.
 */
data class Events(val eventType: String? = null, val bundle: Bundle? = null)