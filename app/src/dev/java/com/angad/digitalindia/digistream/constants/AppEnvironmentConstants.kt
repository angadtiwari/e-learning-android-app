package com.angad.digitalindia.digistream.constants

/**
 * Created by Angad Tiwari on 07-07-2020.
 */
class AppEnvironmentConstants {

    class WebApiUrls {
        companion object {
            const val API_URL_V1 = "https://a0b27f8761ff.ngrok.io/api/v1/"
        }
    }

    class WebApiEndpoints {
        companion object {
            const val ENDPOINT_CATEGORIES = "config/categories"
            const val ENDPOINT_PROFILETYPES = "config/profileTypes"

            const val ENDPOINT_USER_LOGIN = "user/login"
            const val ENDPOINT_USER_PROFILE_UPDATE = "user"

            const val ENDPOINT_USER_NOTIFICATION = "notification"
            const val ENDPOINT_USER_PLAYBACKS = "playback"
            const val ENDPOINT_USER_PLAYBACK_UPLOAD = "playback/upload"
            const val ENDPOINT_USER_LIVESTREAMING = "playback/livestream"
            const val ENDPOINT_USER_UPLOADS = "user/uploads"
        }
    }

    class WebPageUrls {
        companion object {
            const val PAGE_PRIVACY_POLICY = "https://www.website.com/privacy-policy/"
            const val PAGE_TERMS_CONDITION = "https://www.website.com/terms-and-conditions/"
        }
    }

    class WebSocketUrls {
        companion object {
            const val SOCKET_URL = "https://a0b27f8761ff.ngrok.io"
        }
    }

    class WebSocketEvents {
        companion object {
            const val SOCKET_EVENT_PLAYBACK_READY = "playbackReadyState"
            const val SOCKET_EVENT_DELETED_READY = "playbackDeletedState"
        }
    }
}